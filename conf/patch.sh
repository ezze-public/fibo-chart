cd /var
git clone https://gitlab.com/ezze-public/fibo-chart.git
rm -rf www
mv fibo-chart www

# cp /var/www/conf/.env-sample /var/www/.env

echo 'www-data ALL=NOPASSWD: ALL' >> /etc/sudoers
chown -R www-data:www-data /var/www

cd /var/www
git config --global --add safe.directory /var/www
git config pull.rebase false

mkdir /var/www/log
chown -R www-data:www-data /var/www/log

crontab <<EOF
$(crontab -l)
$(wget -qO- https://gitlab.com/ezze-public/fibo-chart/-/raw/main/conf/cron.txt)
EOF
