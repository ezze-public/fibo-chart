
var last_updated_in_form = 'budget_in_use';

jQuery(document).ready(function($) {
	
    // take care of first_step in form
    $('input[name="budget_in_use"]').on('keyup', function() {
        last_updated_in_form = 'budget_in_use';
        sync_form();
    });
    $('input[name="first_step"]').on('keyup', function() {
        last_updated_in_form = 'first_step';
        sync_form();
    });
    // $('input[name="init_position"]').on('click', function() {
    //     sync_form();
    // });
    $('input[name="max_step"]').on('change', function() {
        sync_form();
    });

    sync_form();

});

function sync_form(){

    var form = $('form.botlist_newForm');
    // var init_position = form.find('input[name="init_position"]').is(':checked');
    var fibo = sum_of_fibo( $('input[name=max_step]').val() );

    if( last_updated_in_form == 'budget_in_use'){
        var budget_in_use = form.find('input[name="budget_in_use"]').val();
        var first_step = budget_in_use / fibo.sum; // (init_position ? 22 : 20);
        first_step = Math.round(first_step*100) / 100;
        form.find('input[name="first_step"]').val(first_step);
    
    } else if( last_updated_in_form == 'first_step'){
        var first_step = form.find('input[name="first_step"]').val();
        var budget_in_use = first_step * fibo.sum;// (init_position ? 22 : 20);
        budget_in_use = Math.round( budget_in_use * 100) / 100;
        form.find('input[name="budget_in_use"]').val(budget_in_use);
    
    }
}

jQuery(document).ready(function($) {
    $('input[name=max_step]').on('change', function(){

        var t = $(this);
        var m = t.parent().find('memo');

        $('input[name=valid_step]').attr('max', t.val()).val(t.val());

        res = sum_of_fibo(t.val());
        m.html( res.memo + ' : ' + res.sum );

    });

});

function sum_of_fibo( n ){

    var memo = new Array;
    var sum = new Array;
    var sum_of_all = 1;

    memo.push(1);
    sum[-1] = 0;
    sum[0] = 1;
    
    for( var i = 1; i<n; i++ ){
        sum[i] = sum[i-2] + sum[i-1];
        memo.push(sum[i]);
        sum_of_all+= sum[i];
    }

    var res = new Object;
    res.memo = memo.join(', ');
    res.sum = sum_of_all;

    return res;

}

jQuery(document).ready(function($) {
    $('.timerange select').on('change', function(){
        if( $(this).val() == 'c' ){
            $(this).parent().find('.customrange_form').show();
        } else {
            $(this).parent().find('.customrange_form').hide();
        }
    });
});


jQuery(document).ready(function($) {
    $('.timerange input[type=button]').on('click', function(){

        if(! $(this).attr('disabled') ){
            
            var t = $(this);
            var ff = t.closest('form');

            var bot_id = ff.attr('bot_id');
            var symbol = ff.find('input[name=symbol]').val();
            var leverage = ff.find('input[name=leverage]').val();
            var budget = ff.find('input[name=budget_in_use]').val() / 100;
            var step_gap = ff.find('input[name=step_gap]').val();
            var max_step = ff.find('input[name=max_step]').val();
            var valid_step = ff.find('input[name=valid_step]').val();
            
            var date_style = ff.find('select[name=date_style]').val();
            var date_string = '&date_style='+date_style;
            if( date_style == 'c' ){
                var date_from = ff.find('input[name=date_from]').val();
                var date_to = ff.find('input[name=date_to]').val();
                if( !date_from || !date_to ){
                    alert('invalid date format');
                    exit;
                }
                date_string+= '&date_from='+date_from+'&date_to='+date_to;
            }

            if( date_from > date_to ){
                alert('Wrong date range selected.');

            } else {
                
                t.attr('disabled', true);
                
                var the_url = './?do_action=backtest_save&bot_id='+bot_id+'&symbol='+symbol+'&leverage='+leverage+'&budget='+budget+'&step_gap='+step_gap+'&max_step='+max_step+'&valid_step='+valid_step + date_string;
                // console.log(the_url)

                $.ajax({ url: the_url }).done(function(code) {

                    if( code.status == 'OK' ){

                        if( code.msg == 'processing' ){ // from local db, not fcb json
                            $('.timerange .fa-solid.fa-vials').removeClass('old');// .addClass('moving').removeClass('moving');

                        } else {
                            alert('Duplicated query.');
                        }

                    } else {
                        console.log('something wrong');
                    }

                    t.removeAttr('disabled');

                });

            }

        }
    });
});



jQuery(document).ready(function($) {

    $('.fa-solid.fa-vials').on('click', function(){
        $(this).addClass('old');
    })

});


function botform_timerange_feedCheck(){
jQuery(document).ready(function($) {
    
    var ff = $('form.botlist_newForm')
    var symbol = ff.find('input[name=symbol]').val()

    var tr = ff.find('.timerange')
    var tr_f = tr.find('.the_form')
    var tr_m = tr.find('.the_messagebox')
    
    tr_f.hide()
    tr_m.html('loading ..')
    tr_m.show()

    $.ajax({ url: './?do_action_pre=cmh_available&symbol='+symbol+'&tf=1m' }).done(function(code) {
        
        // console.log(code.status)

        if( code.status == 'problem' ){
            alert(code.msg)

        } else if( code.status == 'pending' ){
            tr_m.html('Retriving data from CMH.')

        } else if( code.status == 'perfect' ){
            tr_m.hide()
            tr_f.show()
        }

    })

})
}

jQuery(document).ready(function($) {
    botform_timerange_feedCheck();
})

function dropsearch_run_after_select_option(){
    // console.log('dropsearch_run_after_select_option is working ..');
    botform_timerange_feedCheck();
}
