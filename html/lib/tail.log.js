
function do_reset_proc_ptr(){
jQuery(document).ready(function($) {
    
    $.ajax({ url: './?do_action_pre=reset_proc_ptr&proc='+proc+'&proc_ptr='+proc_ptr+'&proc_ptr_i='+proc_ptr_i }).done(function(res){

        var class_extra;
        var text_arr;

        if( res.status == 'OK' ){
            if( res.proc_ptr_i == proc_ptr_i ){
                if( res.text ){
                    
                    text_arr = res.text.split(/\r?\n|\r|\n/g);
                    class_extra = '';

                    for( const line of text_arr ){
                        class_extra = ( line.includes('CLOSE WITH') ? ' close' : '' );
                        $('ul').prepend('<li class="new'+class_extra+'" >'+line+'</li>');
                    }

                    proc_ptr = res.proc_ptr;
                    // console.log('new proc_ptr: '+ proc_ptr);

                    sync_bar_etc();

                }
            }
        
        } else {
            console.log(res.code ? res.code : res);
        }
    });

});
}

function sync_bar_price(){
jQuery(document).ready(function($) {
    $('bar > price > curr, bar > profit').addClass('loading');
    var proc = $('body').attr('proc');
    $.ajax({ url: './?do_action_pre=sync_bar_price&proc='+proc }).done(function(res){
        if( res.status == 'OK' ){

            $('bar > price > curr').html(res.price);

            $('bar > profit > round > amount').html(res.round_profit);
            $('bar > profit > round > percent').html(res.round_percent);
            $('bar > profit > round').removeClass().addClass(res.round_class);

            $('bar > profit > total > amount').html(res.total_profit);
            $('bar > profit > total > percent').html(res.total_percent);
            $('bar > profit > total').removeClass().addClass(res.total_class);

            $('bar > price > curr, bar > profit').removeClass('loading');

        }
    });
 });
}

function sync_bar_etc(){
jQuery(document).ready(function($) {
    $('bar > price > down, bar > price > up').addClass('loading');
    var proc = $('body').attr('proc');
    $.ajax({ url: './?do_action_pre=sync_bar_etc&proc='+proc }).done(function(res){
        if( res.status == 'OK' ){

            $('bar > price > down').html(res.down);
            $('bar > price > up').html(res.up);
            
            if( res.liq > 0 )
                $('bar > price > liq').html(res.liq);

            $('bar > price > down, bar > price > up').removeClass('loading');

        }
    });
});
}


var proc_ptr_i = 0;
do_reset_proc_ptr();

sync_bar_price();
sync_bar_etc();

setInterval(function () {
    
    if( document.hidden ){
        console.log('...');
    
    } else {
        
        proc_ptr_i++;
        console.log('try in '+proc_ptr_i);

        sync_bar_price();
        // sync_bar_etc();
        do_reset_proc_ptr();

    }

}, 4000);


