

function do_syncAPIList_once(){
    jQuery(document).ready(function($) {
    
        $('.botlist > .re.r').each( function(){
            do_syncAPIList_this( $(this).attr('bot') );
        });
    
    });
}
do_syncAPIList_once();


function do_syncAPIList(){
    jQuery(document).ready(function($) {
    
        $('.botlist > .re.r:not(.na)').each( function(){
            do_syncAPIList_this( $(this).attr('bot') );
        });
    
    });
}

function do_syncAPIList_this( bot ){
jQuery(document).ready(function($) {

    var t = $('.botlist .re.r[bot='+bot+']');

    $.ajax({ url: './?do_action=bot_stat&bot='+bot }).done(function(res){
        
        console.log(bot);
        console.log(res);
        
        if( res.errMsg )
            t.find('.name').prepend('<e>'+res.errMsg+'</e>');

        var open_s = '';
        var pl = Object.keys(res.positions).length;
        var ol = Object.keys(res.orders).length;
        
        if( pl > 0 )
            open_s = open_s + pl + ' pos';
        
        if( ol > 0 ){
            if( pl > 0 )
                open_s = open_s + ', ';
            open_s = open_s + ol + ' ord';
        }

        if( res.newlog > 0 ){
            t.find('.log_a i').addClass('newlog');
        } else {
            t.find('.log_a i').removeClass('newlog');
        }
        
        t.find('.stat').html('<stat></stat><o></o>');
        t.find('.stat o').html(open_s);

        do_syncAPIList_setStat(t, res.status);
        
    });
});
}


jQuery(document).ready(function($) {

    $('.botlist').delegate(".newlog", "click", function() {
        console.log('hihaa');
        $(this).removeClass('newlog');
    });
});

    
function do_syncAPIList_setStat( t, stat ){
jQuery(document).ready(function($) {

    // console.log(stat);
    t.find('.stat stat').html(stat);
        
    switch( stat ){
        
        case 'starting':
        case 'started':
            t.find('.tools>.remove, .tools>.start').addClass('disabled');
            t.find('.tools>.stop, .tools>.safestop, .tools>.terminate, .tools>.disconnect').removeClass('disabled');
            t.find('.tools>.start').removeClass('half');
            break;

        case 'stopping':
        case 'safestopping':
            t.find('.tools>.terminate, .tools>.disconnect, .tools>.start').removeClass('disabled');
            t.find('.tools>.remove, .tools>.stop, .tools>.safestop').addClass('disabled');
            t.find('.tools>.start').addClass('half');
            break;

        case 'disconnecting':
        case 'terminating':
            t.find('.tools>.remove, .tools>.start, .tools>.stop, .tools>.safestop, .tools>.terminate, .tools>.disconnect').addClass('disabled');
            t.find('.tools>.start').removeClass('half');
            break;

        case 'stopped':
            t.find('.tools>.stop, .tools>.safestop, .tools>.terminate, .tools>.disconnect').addClass('disabled');
            t.find('.tools>.remove, .tools>.start').removeClass('disabled');
            t.find('.tools>.start').removeClass('half');
            break;

    }
});
}


jQuery(document).ready(function($) {
    do_syncAPIList();
    setInterval(function(){
        if(! document.hidden )
            do_syncAPIList();
    }, 5000);
});


jQuery(document).ready(function($) {

    $('.botlist .re.r .spot').on('click', function(){
        
        var move = 0;
        var t = $(this);
        var r = t.closest('.r');
        var b = r.find('.balance');
        var s = t;
        var bot = r.attr('bot');

        $.ajax({ url: './?do_action=bot_stat&bot='+bot }).done(function(res){

            var b_val = parseFloat(b.html());
            var s_val = parseFloat(s.html());

            var bot_status = res['status'];

            if( s_val <= 0 ){
                alert('No balance to transfer.');

            } else if( bot_status == 'started'){
                
                move = prompt('How much to lend? (will be back after closing the current trade.)', s_val);
                if( move > 0 ){
                    
                    $.ajax({ url: './?do_action=transfer-spare&id='+bot+'&amount='+move }).done(function(res) {
                        // console.log(res);
                        if( res['status'] == 'OK' ){
                            var moved = parseFloat(res['log']);
                            t.html( parseFloat(s_val - moved).toFixed(2) );
                            b.html( parseFloat(b_val + moved).toFixed(2) );
                            alert(moved+' USDT lended successfully.');

                        } else {
                            alert(res['log']);
                        }

                    });

                } else {
                    alert('Cancelled.');
                }

            } else if( bot_status == 'stopped' ){
                
                move = prompt('How much to transfer? (manual transfer)', s_val);
                if( move > 0 ){
                    $.ajax({ url: './?do_action=transfer-move&target=contract&id='+bot+'&amount='+move }).done(function(res) {
                        // console.log(res);                        
                        if( res['status'] == 'OK' ){
                            var moved = parseFloat(res['log']);
                            b.html( parseFloat(b_val + moved).toFixed(2) );
                            s.html( parseFloat(s_val - moved).toFixed(2) );
                            alert(moved+' USDT transfered to Perpetual Wallet successfully.');

                        } else {
                            alert(res['log']);
                        }

                    });

                } else {
                    alert('Cancelled.');
                }

            } else {
                alert('Can\'t transfer balance while the bot is not in stabilized state: '+bot_status);
            }

        });

    });

    $('.botlist .re.r .balance').on('click', function(){
        
        var move = 0;
        var t = $(this);
        var b = t;
        var s = t.closest('.r').find('.spot');
        var re = t.closest('.re');
        var bot = re.attr('bot');

        $.ajax({ url: './?do_action=bot_stat&only_status=1&bot='+bot }).done(function(json){
                        
            var b_val = parseFloat(b.html());
            var s_val = parseFloat(s.html());

            if( b_val <= 0 ){
                alert('No balance to transfer.');

            } else if( json.status == 'stopped' ){

                move = prompt('How much to transfer? (manual transfer)', b_val);
                if( move > 0 ){
                    $.ajax({ url: './?do_action=transfer-move&target=spot&id='+bot+'&amount='+move }).done(function(res) {
                                                
                        if( res.status == 'OK' ){
                            var moved = parseFloat(res['log']);
                            b.html( parseFloat(b_val - moved).toFixed(2) );
                            s.html( parseFloat(s_val + moved).toFixed(2) );
                            alert(moved+' USDT transfered to Spot Wallet successfully.');

                        } else {
                            alert(res['log']);
                        }

                    });

                } else {
                    alert('Cancelled.');
                }

            } else {
                alert('Can\'t transfer balance from Perpetual Wallet while the bot is started.');
            }

        });

    });

});


