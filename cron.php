<?php


chdir('/var/www');
include_once('inc/.php');


if(! file_exists('/tmp/perp-fibo-trader') )
	shell_exec(" sudo mkdir /tmp/perp-fibo-trader; sudo chown -R www-data:www-data /tmp/perp-fibo-trader; sudo chmod -R 0777 /tmp/perp-fibo-trader ");


if( $argv[1] and $argv[1] == '--backtest' ){
    define('TEST_ID', $argv[2]);
    return backtest::handle();
}


# php -q /var/www/cron.php :23: <- bot_id
# it goes to cron_sub
if( $argv[1] and $argv[1] != '--cron' ){
    
    define('BOT_ID', substr($argv[1], 1, -1));
    cron_sub();

# php -q /var/www/cron.php --cron
} else {
    
    # if debug mode, then dont run the cron
    if( $argv[1] and $argv[1] == '--cron' and defined('DEBUG_MODE') and DEBUG_MODE == true )
        die;

    cron_main();

}

