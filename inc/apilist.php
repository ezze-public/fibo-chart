<?php


function apilist(){

    switch( $_REQUEST['do2'] ){

        case 'form':
            return apilist_form();

        case 'saveNew':
            apilist_saveNew();
            break;

        case 'saveEdit':
            apilist_saveEdit();
            break;
        
        case 'delete':
            apilist_delete();
            break;

    }

    echo "<div class='apilist'>";

    echo "<div class='re h' >";
    echo "<span class='name' >name</span>";
    echo "<span class='ex' >exchange</span>";
    echo "<span class='key' >key</span>";
    echo "<span class='bots' >bots</span>";
    echo "<span class='actives' >active</span>";

    echo "<div class='balance'>balance</div>";
    echo "<div class='spot'>spot</div>";
    echo "<div class='total'>total</div>";

    echo "<span class='action' >--</div>";

    $sum_balance = 0;
    $sum_spot = 0;
    $sum_total = 0;
    
    if(! $user_id = login_id() ){
        die('no access');

    } else if(! $rs = dbq(" SELECT * FROM `api` WHERE `user`=$user_id ") ){
        echo dbe();
    
    } else if(! dbn($rs) ){
        echo "<br><br><br><br><br><div style=text-align:center >click <a href=\"./?do=api&do2=form\">here</a> to create your first bot.</div><br><br><br><br><br>";

    } else while( $rw = dbf($rs) ){
        
        extract($rw);
        $ex = $api_ex;
        $credentials = $api_key.':'.$api_secret;

        $numb_of_bots = dbr( dbq(" SELECT COUNT(*) FROM `bot` WHERE `api`=$id "), 0, 0);
        $active_of_bots = 0; // rand(2, 3);

        $balance = ($ex)::balance($credentials, 'USDT', false);

        if( $balance === 'N/A' ){
            $balance = $spot = $total_balance = 'N/A';
        
        } else {

            $balance = number_format( $balance, 2, '.', '');
            $spot = ($ex)::spot_balance($credentials, 'USDT', false);
            
            $spot = number_format( $spot, 2, '.', '');
            $total_balance = number_format($balance + $spot, 2, '.', '');
            
            $sum_balance+= $balance;
            $sum_spot+= $spot;
            $sum_total+= $total_balance;

        }

        echo "<div class='re r' >";
        echo "<span class='name' >".$api_name."</span>";
        echo "<span class='ex'>".$api_ex."</span>";
        echo "<span class='key' >".code_brief($api_key, 3)."</span>";
        echo "<span class='bots' >$numb_of_bots</span>";
        echo "<span class='actives' >$active_of_bots</span>";

        echo "<div class='balance'>{$balance}</div>";
        echo "<div class='spot'>{$spot}</div>";
        echo "<div class='total'>{$total_balance}</div>";

        echo "<span class='action' >";
        echo '<a class="mx-1" href="./?do=api&do2=form&id='.$id.'" title="edit"><i class="fa fa-pencil-square" aria-hidden="true"></i></a>';
        echo '<a class="mx-1" '.($numb_of_bots ?'title="it does have active bots"' :'href="./?do=api&do2=delete&id='.$id.'" title="delete"').' onclick="if(! confirm(\'Are you sure?\') ) return false;"><i class="fa fa-trash" aria-hidden="true"></i></a>';
        echo '<a title="new bot on this api" href="./?do=bot&do2=form&api='.$id.'"><i class="fa-regular fa-square-plus"></i></a>';
        echo "</span>";
        echo "</div>";

    }

    echo '<div class="re r" >';
    echo '<div class="name"></div>';
    echo '<span class="ex"></span>';
    echo '<span class="key"></span>';
    echo '<span class="bots"></span>';
    echo '<span class="actives"></span>';

    echo "<div class='balance'>{$sum_balance}</div>";
    echo "<div class='spot'>{$sum_spot}</div>";
    echo "<div class='total'>{$sum_total}</div>";

    echo '<span class="action"><a title="add" href="./?do=api&do2=form"><i class="fa-solid fa-square-plus text-blue-500"></i></a></span>';
    echo '</div><!-- .r -->';
    echo '</div><!-- .apilist -->';

}


function apilist_form(){

    if( $user_id = login_id() )
        if( $id = $_GET['id'] )
            if( $rs = dbq(" SELECT * FROM `api` WHERE `user`=$user_id AND `id`=$id LIMIT 1 ") )
                if( dbn($rs) )
                    $rw = dbf($rs);

    ?>
    <form name="apilist_form" autocomplete="off" class="apilist_form" method="post" action="./?do=api&do2=<?php echo ( isset($rw) ? 'saveEdit&id='.$rw['id'] : 'saveNew' ) ?>" >
    
    <div><span>name</span><input autocomplete="off" type="text" name="api_name" value="<?php echo ( isset($rw) ? $rw['api_name'] : '' ) ?>" required /></div>
    <div><span>exchange</span><select name="api_ex" required ><option></option><option value="binance">Binance</option><option value="bybit">ByBit</option><option value="mexc">MEXC</option></select></div>
    <?php echo ( isset($rw) ? "<script>apilist_form.api_ex.value = '".$rw['api_ex']."';</script>" : '' ) ?>
    <div><span>api key</span><input autocomplete="off" type="text" name="api_key" value="<?php echo (isset($rw) ? $rw['api_key'] : '') ?>" required /></div>
    <div><span>api secret</span><input autocomplete="off" type="text" name="api_secret" value="<?php echo (isset($rw) ? $rw['api_secret'] : '') ?>" required /></div>
    <div><span>api wallet</span><input autocomplete="off" type="text" class="" name="api_wallet" value="<?php echo (isset($rw) ? $rw['api_wallet'] : '') ?>" /></div>
    <div><span></span><input type="submit" value="save"/></div>

    </form>
    <?php
}


function apilist_saveNew(){
    
    if(! $user_id = login_id() ){
        die('no access');

    } else {
        
        $api_name = trim($_POST['api_name']);
        $api_ex = trim($_POST['api_ex']);
        $api_key = trim($_POST['api_key']);
        $api_secret = trim($_POST['api_secret']);
        $api_wallet = trim($_POST['api_wallet']);

        if( !$api_name or !$api_ex or !$api_key or !$api_secret ){
            echo "something wrong";

        } else if( dbn(dbq(" SELECT * FROM `api` WHERE `user`=$user_id AND `api_name`='$api_name' ")) ){
            echo "api name \"{$api_name}\" already in use";

        } else if( dbn(dbq(" SELECT * FROM `api` WHERE `user`=$user_id AND `api_key`='$api_key' ")) ){
            echo "api key \"{$api_name}\" already in use";

        } else if(! dbq(" INSERT INTO `api` (`user`, `api_name`, `api_ex`, `api_key`, `api_secret`, `api_wallet`) VALUES ($user_id, '$api_name', '$api_ex', '$api_key', '$api_secret', '$api_wallet') ") ){
            echo dbe();

        } else {
            do_redirect('./?do=api');
        }

    }

}


function apilist_saveEdit(){
    
    if(! $user_id = login_id() ){
        die('no access');

    } else {
        
        $id = $_GET['id'];
        $api_name = trim($_POST['api_name']);
        $api_ex = trim($_POST['api_ex']);
        $api_key = trim($_POST['api_key']);
        $api_secret = trim($_POST['api_secret']);
        $api_wallet = trim($_POST['api_wallet']);

        if( !$id or !$api_ex or !$api_key or !$api_secret ){
            echo "something wrong";

        } else if( dbn(dbq(" SELECT * FROM `api` WHERE `user`=$user_id AND `api_name`='$api_name' AND `id`!=$id ")) ){
            echo "api name \"{$api_name}\" already in use";

        } else if( dbn(dbq(" SELECT * FROM `api` WHERE `user`=$user_id AND `api_key`='$api_key' AND `id`!=$id ")) ){
            echo "api key \"{$api_name}\" already in use";

        } else if(! dbq(" UPDATE `api` SET `api_name`='$api_name', `api_ex`='$api_ex', `api_key`='$api_key', `api_secret`='$api_secret', `api_wallet`='$api_wallet' WHERE `user`=$user_id AND `id`=$id LIMIT 1 ") ){
            echo dbe();

        } else {
            do_redirect('./?do=api');
        }

    }

}


function apilist_delete(){
    
    if(! $user_id = login_id() ){
        die('no access');

    } else if(! $id = intval($_GET['id']) ){
        echo "something wrong";
    
    } else if( dbr( dbq(" SELECT COUNT(*) FROM `bot` WHERE `api`=$id "), 0, 0) > 0 ){
        echo 'it does have active bots';
        
    } else if(! dbq(" DELETE FROM `api` WHERE `user`=$user_id AND `id`=$id LIMIT 1 ") ){
        echo dbe();

    } else {
        do_redirect('./?do=api');
    }
}


function code_brief( $code, $n=2 ){

    $code = substr($code, 0, $n) . '..' . substr($code, -1*$n);
    return $code;

}

