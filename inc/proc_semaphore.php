<?php

# v-0.2 2022-11-20

function proc_semaphore( $incl=[], $excl=[] ){

    if(! is_array($incl) )
        $incl = [ $incl ];

    if(! is_array($excl) )
        $excl = [ $excl ];

    $cmd = " ps aux | grep -v grep ";
    
    if( sizeof($incl) )
    foreach( $incl as $c ){
        $c = str_replace("'", "\'", $c);
        $c = trim($c, "\r\n\t ");
        $cmd.= "| grep '{$c}' ";
    }

    if( sizeof($excl) )
    foreach( $excl as $c ){
        $c = str_replace("'", "\'", $c);
        $c = trim($c, "\r\n\t ");
        $cmd.= "| grep -v '{$c}' ";
    }

    $cmd.= "| awk {'print \$2'} ";
    $cmd.= "| grep -v ".getmypid();
    $cmd.= "| wc -l ";

    while( true ){
        
        $res = shell_exec($cmd);
        $res = trim($res, "\r\n\t ");

        if( $res == 0 )
            break;
        
        if( $res > 1 ) // if some one else is waiting
            die;

        if( ++$semaphore_k >= 30 ) // if I've been waiting for more than 30 seconds
            die;
        
        sleep(1);

    }

}