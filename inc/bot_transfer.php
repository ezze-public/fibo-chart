<?php



function bot_transfer_move(){
    
    if(! $user = login_id() )
        die;

    $status = 'ER';
    $id = intval($_GET['id']);
    $target = $_GET['target'];
    $amount = floatval($_GET['amount']);

    if(! $rs = dbq(" SELECT `symbol`, `api_key`, `api_secret` FROM `bot` WHERE `user`=$user AND `id`=$id LIMIT 1 ") ){
        $log = dbe();
        $log.= " SELECT `symbol`, `api_key`, `api_secret` FROM `bot` WHERE `user`=$user AND `id`=$id LIMIT 1 ";
    
    } else if(! $rw = dbf($rs) ){
        $log = "no row found";
    
    } else {

        extract($rw);

        $credentials = $api_key.':'.$api_secret;
        
        list($coin, $base) = explode('_', $symbol);
        $balance = ( $target == 'spot' )
            ? bybit::balance($credentials, $base, false)
            : bybit::spot_balance($credentials, $base, false);

        $amount = min($amount, $balance);
        
        if(! $amount ){
            $log = "no balance to transfer";
        
        } else if(! bybit::transfer($credentials, $base, $amount, strtoupper($target)) ){
            $log = "Can't transfer";

        } else {
            $log = $amount;
            $status = 'OK';
        }
        
    }

    return ['status'=>$status, 'log'=>$log];

}



function bot_transfer_spare(){
    
    if(! $user = login_id() )
        die;
        
    $status = 'ER';
    $id = intval($_GET['id']);
    $amount = floatval($_GET['amount']);

    if(! $rs = dbq(" SELECT `symbol`, `api_key`, `api_secret` FROM `bot` WHERE `user`=$user AND `id`=$id LIMIT 1 ") ){
        $log = dbe();
    
    } else if(! $rw = dbf($rs) ){
        $log = "no row found";
    
    } else {

        extract($rw);
        
        $credentials = $api_key.':'.$api_secret;
        
        list($coin, $base) = explode('_', $symbol);
        $spot_balance = bybit::spot_balance($credentials, $base, false);

        $amount = min($amount, $spot_balance);

        if(! $amount ){
            $log = "no balance to transfer";
        
        } else if(! bybit::transfer($credentials, $base, $amount, 'CONTRACT') ){
            $log = "Can't transfer";

        } else if(! dbq(" UPDATE `bot` SET `spare`=`spare`+$amount WHERE `user`=$user AND `id`=$id LIMIT 1 ") ){
            $log = dbe();

        } else {
            $log = $amount;
            $status = 'OK';
        }
        
    }

    return ['status'=>$status, 'log'=>$log];

}


