<?php


function bot_remove(){
    if(! $user = login_id() ){
        echo "access denied";
    } else if(! $id = intval($_GET['id']) ){
        echo "no id defined";
    } else if(! dbq(" DELETE FROM `bot` WHERE `id`=$id AND `status` IN ('', 'stopped') AND `api` IN (SELECT `id` FROM `api` WHERE `user`=$user) LIMIT 1 ") ){
        echo dbe();
    } else if(! dbaf() ){
        echo "somethig wrong";
    } else {
        echo "OK";
        echo "#eval";
        echo '$(".re.r[bot='.$id.']").remove();';
        echo "#alert";
        echo "done";
    }
}


function bot_stat(){
    
    $res = [ 'status' => 'stopped' ];
    
    if(! array_key_exists('only_status', $_GET) ){
        $res['orders'] = [];
        $res['positions'] = [];
    }
    if( $bot = intval($_GET['bot']) ){
        extract(bot_rw($bot));
        $res['status'] = $status ? : 'stopped';
        if(! array_key_exists('only_status', $_GET) ){
            $credentials = $api_key.':'.$api_secret;
            
            $res['orders'] = bybit::order_list($credentials, $symbol);
            if( $GLOBALS['bybit']['errMsg'] ){
                $res['errMsg'] = $GLOBALS['bybit']['errMsg'];

            } else {
                $res['positions'] = bybit::position_list($credentials, $symbol);
                if( $GLOBALS['bybit']['errMsg'] )
                    $res['errMsg'] = $GLOBALS['bybit']['errMsg'];
            }

            $res['newlog'] = 0;

            if( $rw = dbqf(" SELECT `id` FROM `proc` WHERE `bot`=$bot ORDER BY `id` DESC LIMIT 1 ") ){
                $proc = $rw['id'];
                $res['newlog'] = dbr(dbq(" SELECT COUNT(*) FROM `log` WHERE `proc`=$proc AND `seen`=0 "),0,0);
            }
        }
    }

    return $res;
    
}


function fibo_serial( $max ){
    
    $f[0] = 0;
    $f[1] = 1;

    for( $step=2; $step<=$max; $step++ ){
        $f[$step] = intval($f[$step-2]) + intval($f[$step-1]);
        $last = $f[ $step ];
    }

    return $f;

}


function bot_rw( $bot, $set=null ){

    if( !is_null($set) and is_array($set) ){
        
        $changes = [];
        foreach( $set as $k => $v )
            if(! in_array($k, ['id', 'user']) )
                $changes[] = " `$k`='$v' ";
        
        if( sizeof($changes) )
            dbq(" UPDATE `bot` SET ".implode(", ", $changes)." WHERE `id`={$bot} LIMIT 1 ");
    
    }

    $rw = dbqf(" SELECT * FROM `bot` WHERE `id`={$bot} LIMIT 1 ");

    if( !is_null($set) and !is_array($set) ){
        return $rw[$set];

    } else {
        return $rw;
    }

}


function proc_rw( $set=null, $PROC_ID=null ){
    
    if( !$PROC_ID and defined('PROC_ID') )
        $PROC_ID = PROC_ID;

    if(! $PROC_ID ){
        return false;

    } else {

        if( !is_null($set) and is_array($set) ){
            $changes = [];
            foreach( $set as $k => $v )
                if(! in_array($k, ['id', 'bot']) ){

                    if( is_null($v) ){
                        $v = 'NULL';

                    } else if(! is_numeric($v) ){
                        $v = "'{$v}'";
                    }

                    $changes[] = " `$k`={$v} ";

                }
                
            if( sizeof($changes) ){
                
                $query = " UPDATE `proc` SET ".implode(", ", $changes)." WHERE `id`={$PROC_ID} LIMIT 1 ";
                log_this('sys', __FUNCTION__.':'.__LINE__.': '.$query );

                if(! dbq($query) )
                    log_this('logg', __FUNCTION__.':'.__LINE__.': '.dbe() );

            }

        }

        $rw = dbqf(" SELECT * FROM `proc` WHERE `id`={$PROC_ID} LIMIT 1 ");

        if( !is_null($set) and !is_array($set) ){
            return $rw[$set];

        } else {
            return $rw;
        }

    }

}


