<?php

# v-0.1 2022-11-14

function login_care(){

    if( login_id() or login_do() ){
        return true;
    
    } else {
        login_form();
        die;
    }

}


function login_form(){
    
    if( function_exists('html_tag_open') )
        html_tag_open();
    
    ?>
    <form method="post" action="" class="login">
        <div class="re"><input type="text" name="username"/></div>
        <div class="re"><input type="password" name="password"/></div>
        <div class="r0e"><input type="submit" value="login" /></div>
    </form>
    <script>
        jQuery(document).ready(function($) {
            $('form input[name=username]').focus();
        });
    </script>
    <?php

    if( function_exists('html_tag_close') )
        html_tag_close();

}


function login_try(){
    
    if( isset($_POST['username']) and isset($_POST['password']) ){

        $username = $_POST['username'];
        $password = $_POST['password'];

        $rs = dbq(" SELECT `id` FROM `user` WHERE `username`='{$username}' AND `password`='{$password}' LIMIT 1 ");
        
        if( dbn($rs) ){
            $user_id = dbr($rs, 0, 0);
            setcookie( 'login_cookie_token', json_encode([ 'user_id'=>$user_id, 'sign'=>login_cookie_sign($user_id) ]), time()+ 3600*24*365 );
            return $user_id;

        } else {
            return false;
        }

    } else if( array_key_exists('login_cookie_token', $_COOKIE) ){
        
        extract( json_decode($_COOKIE['login_cookie_token'], true) );
        
        if( $user_id and $sign )
            if( login_cookie_sign($user_id) == $sign )
                return $user_id;
        
    }
    
    return false;

}


function login_do(){

    if( $user_id = login_try() ){
        $_SESSION['user'] = $user_id;
        echo "<script>location.href = './';</script>";
        die;
    }
    
    return false;

}


function login_cookie_sign( $user_id ){
    return sha1($user_id.'-'.__FUNCTION__);
}


function login_id(){
    return isset($_SESSION['user'])
        ? $_SESSION['user']
        : null;
}


function login_exit(){

    setcookie('login_cookie_token', '', time()-3600);
    unset($_COOKIE['login_cookie_token']);
    $_SESSION['user'] = null;

    echo "<script>location.href = './';</script>";
    die;

}


function login_info(){

    if( $user_id = login_id() )
        if( $rs = dbq(" SELECT * FROM `user` WHERE `id`={$user_id} LIMIT 1 ") )
            if( dbn($rs) )
                return dbf($rs);
    
    return false;

}


function user_has_access( $table, $id ){
    $user = login_id();
    $rs = dbq(" SELECT * FROM `{$table}` WHERE `user`=$user AND `id`=$id LIMIT 1 ");
    return dbn($rs) ? true : false;
}


