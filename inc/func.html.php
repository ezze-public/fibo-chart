<?php

# v-0.1 2022-11-14

function html_tag_open(){

?><!doctype html>
<html lang="en" dir="ltr">
<head>
    <title>fibo-chart</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.2/css/all.min.css" integrity="sha512-SnH5WK+bZxgPHs44uWIX+LLJAJ9/2PkPKZ5QiAj6Ta86w+fsb2TkcmfRyVX3pBnMFcV7oQPJkl9QevSCWr3W6A==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <link rel="stylesheet" type="text/css" href="./lib/main.css">

    <script src="//code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

    <link rel="stylesheet" type="text/css" href="//cdn.ezze.li/alerty/alerty.css">
    <script type="text/javascript" src="//cdn.ezze.li/alerty/alerty.js"></script>

    <link rel="stylesheet" type="text/css" href="//cdn.ezze.li/do-ajax/do-ajax.css">
    <script type="text/javascript" src="//cdn.ezze.li/do-ajax/do-ajax.js"></script>

    <link rel="stylesheet" type="text/css" href="//cdn.ezze.li/dropsearch/dropsearch.css">
    <script type="text/javascript" src="//cdn.ezze.li/dropsearch/dropsearch.js"></script>
    
    <link rel="stylesheet" type="text/css" href="//cdn.ezze.li/tiny-toys/tiny-toys.css">
    <script type="text/javascript" src="//cdn.ezze.li/tiny-toys/tiny-toys.js"></script>
        
    <script type="text/javascript" src="./lib/main.js"></script>

</head>
<body class="<?php echo ( login_id() ? 'logged' : 'not-logged' ) ?>">

<?php
}



function html_header(){
    ?>
    <menu>
        <?php
        if( function_exists('login_id') ){
            
            if( login_id() ){
                if( defined('HEADER_LINKS') ){
                    $do = isset($_REQUEST['do']) ? $_REQUEST['do'] : 'bot';
                    foreach( explode(',', HEADER_LINKS) as $page )
                        $link_s[] = "<a class=\"".( $do == $page ? 'clicked' : '' )."\" href=\"./?do={$page}\">{$page}</a>";
                }
                echo implode(' | ', $link_s);
            }
        
            echo "<a class=\"right\" href=\"./?do_action_pre=logout\">logout</a>";
        
        }
        ?>
    </menu>
    <?php
}



function html_footer(){
    
}



function html_tag_close(){
    ?>
    <script src="./lib/tail.js"></script>
    </body>
</html><?php
}


