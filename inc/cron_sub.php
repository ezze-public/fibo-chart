<?php


function cron_sub(){
    
    if(! defined('DEBUG_MODE') )
        proc_semaphore(" php :".BOT_ID.": ", "sh -c");

    $rw = bot_rw(BOT_ID);
    $rw_api = api_rw($rw['api']);
    
    if( in_array($rw['status'], ['', 'stopped']) )
        die;

    define('SYMBOL', $rw['symbol']);
    list($coin, $base) = explode('_', SYMBOL);
    define('COIN', $coin);
    define('BASE', $base);

    define('EX', $rw_api['api_ex']);
    define('CREDENTIALS', $rw_api['api_key'].':'.$rw_api['api_secret']);
    define('MAX_STEP', $rw['max_step']);
    define('VALID_STEP', $rw['valid_step'] ? : MAX_STEP );
    define('STEP_GAP', $rw['step_gap']);
    define('LEVERAGE', $rw['leverage']);
    define('BUDGET_IN_USE', $rw['budget_in_use']);
    
    if( $proc = dbqf(" SELECT `id` FROM `proc` WHERE `bot`=".BOT_ID." AND `done`=0 ORDER BY `id` DESC LIMIT 1 ") )
        define('PROC_ID', $proc['id']);
    
    $cron_start = date('U');

    while( true ){
        
        fibo_chart::handle();

        if( defined('DEBUG_MODE') )
            die;

        # if it's more than one minute we started the bot, then don't try it anymore.
        if( date('U') - $cron_start >= 60 )
            die;

        sleep(1);

    }
    
}



