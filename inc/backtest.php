<?php


function backtest_list(){

    backtest_serv();

    echo '<div class="backtest_list">';

    if(! $bot_id = $_GET['id'] ){
        echo 'no bot_id defined';
    
    } else {
        
        // backtest_form();
        
        if(! $rw_s = dbqf(" SELECT * FROM `test` WHERE `bot`={$bot_id} ORDER BY `id` DESC ") ){
            echo 'no record found.';

        } else {

            echo "
            <div class=\"r h\" ><!--
                --><div class=id ></div><!--
                --><div class=date >from</div><!--
                --><div class=date >to</div><!--
                --><div class=date_in >in</div><!--
                --><div class=symbol >symbol</div><!--
                --><div class=budget >budget</div><!--
                --><div class=leverage >leverage</div><!--
                --><div class=step_gap >gap</div><!--
                --><div class=max_step >max</div><!--
                --><div class=valid_step >valid</div><!--
                --><div class=round >round</div><!--
                --><div class=percent >profit</div><!--
                --><div class=risk >liquidation</div><!--
                --><div></div><!--
            --></div>";
            
            foreach( $rw_s as $rw ){
                
                extract($rw);

                $date_from = substr($date_from, 0, 16);
                $date_to = substr($date_to, 0, 16);
                extract(json_decode($bot_specs, true));

                $symbol = str_replace("_", "", $symbol);
                $budget*= 100;

                $date_in = time::sec_to_hour_n_min( strtotime($date_to) - strtotime($date_from) );

                echo "
                <div class=\"r ".( $liq_price ? 'liquidated' : ($done ? 'not-liquidated' : '') )."\"><!--
                    --><a class=id target=_blank href='/?do=list&do2=backtest_log&test_id={$id}'>#{$id}</a><!--
                    --><div class=date >{$date_from}</div><!--
                    --><div class=date >{$date_to}</div><!--
                    --><div class=date_in >{$date_in}</div><!--
                    --><div class=symbol >{$symbol}</div><!--
                    --><div class=budget >{$budget}</div><!--
                    --><div class=leverage >{$leverage}</div><!--
                    --><div class=step_gap >{$step_gap}</div><!--
                    --><div class=max_step >{$max_step}</div><!--
                    --><div class=valid_step >{$valid_step}</div><!--
                    --><div class=round >".($done ? $round : '-')."</div><!--
                    --><div class=percent >".( $liq_price ? '<m>--</m>' : ($done ? $percent."%" : '-' ) )."</div><!--
                    --><div class=risk >".( $liq_price ? '<ld>'.substr($liq_date, 0, 16).'</ld>' : ( $done ? ($risk == 'no risk' ? "<m>{$risk}</m>" : $risk.'<m> away</m>') : '<m>--</m>') )."</div><!--
                    --><div>{$specs}</div><!--
                --></div>";

            }

        }

    }

    echo '</div>';


}

/*
function backtest_form(){

    // disable the form, if there is any live process

    if(! $bot_id = $_GET['id'] )
        echo 'no bot_id defined';

    if( $rs = dbq(" SELECT `id` FROM `test` WHERE `bot`=$bot_id AND `log`='' LIMIT 1 ") and dbn($rs) ){
        // $proc_id = dbr($rs, 0, 0);
        // echo "<script>location.href='/?do_action_pre=log&bot={$bot_id}&proc={$proc_id}';</script>";
        // echo "<div style=padding:40px>We've got an incomplete test process, you have to wait till it end.<br>
        // You can find the log <a target=_blank href=\"/?do_action_pre=log&bot={$bot_id}&proc={$proc_id}\">here</a>.</div>";
        $disable_form = true;
    }

        
    $bot_rw = bot_rw($bot_id);

    if( $date_from = $_POST['date_from'] and $date_to = $_POST['date_to'] )
        backtest_save($bot_rw, $date_from, $date_to);
    
    $sym = strtolower( str_replace('_', '', $bot_rw['symbol']) );
    // $avl = file_get_contents("http://crypto-market-history.ezze.li/available/{$sym}/1m");
    $avl = '{"date_from":"2019-02-08T00:00","date_to":"2023-03-12T23:59"}';
    $avl = json_decode($avl, true);
    extract($avl);
    
    ?>
    <form method="post" action="" class="backtest_form">
        <div>Choose the time range to backtest: </div>
        <input <?php echo ( $disable_form ? 'disabled=1' : '' ) ?> type="datetime-local" name="date_from" value="<?php echo $date_from ?>" />
        <input <?php echo ( $disable_form ? 'disabled=1' : '' ) ?> type="datetime-local" name="date_to" value="<?php echo $date_to ?>" />
        <input <?php echo ( $disable_form ? 'disabled=1' : '' ) ?> type="submit" name="Go!" />
    </form>
    <?php

    
}
*/

function backtest_save(){

    if(! $bot_id = $_GET['bot_id'] ){
        code::json_die(['status'=>'ER', 'line'=>__LINE__, 'msg'=>__FUNCTION__.':'.__LINE__]);
    
    } else {
        
        $bot_specs = [
            'symbol' => $_GET['symbol'],
            'leverage' => $_GET['leverage'],
            'budget' => $_GET['budget'],
            'step_gap'=> $_GET['step_gap'],
            'max_step'=> $_GET['max_step'],
            'valid_step'=> $_GET['valid_step'],
        ];

        if( $_GET['date_style'] == 'c' ){
            $date_from = $_GET['date_from'];
            $date_to = $_GET['date_to'];
            $date_from = str_replace('T', ' ', $date_from).':00';
            $date_to = str_replace('T', ' ', $date_to).':00';
        
        } else {

            $an_hour = 3600;
            $a_day = $an_hour * 24;

            switch( substr($_GET['date_style'], -1) ){
                case 'h':
                    $duration = $an_hour * substr($_GET['date_style'], 0, -1);
                    break;
                case 'd':
                    $duration = $a_day * substr($_GET['date_style'], 0, -1);
                    break;
            }
            
            $date_lvlb = strtotime( gmdate('Y-m-d', gmdate('U') - $a_day).'T23:59' );
            $date_from = gmdate('Y-m-d H:i:00', $date_lvlb - $duration );
            $date_to = gmdate('Y-m-d H:i:00', $date_lvlb);

        }

        // check if the request is repeated
        $rs_rp = dbq(" SELECT `path` FROM `test` WHERE `bot`=${bot_id} AND `date_from`='${date_from}' AND `date_to`='${date_to}' AND `bot_specs`='".json_encode($bot_specs)."' LIMIT 1 ");
        if( dbn($rs_rp) ){
            $rw = dbf($rs_rp);
            extract($rw);
            code::json_die(['status'=>'OK', 'line'=>__LINE__, 'msg'=>'processed', 'path'=>$path]);
        }
        
        if(! $test_id = dbin('test', [ 'bot'=>$bot_id, 'bot_specs'=>json_encode($bot_specs), 'date_from'=>$date_from, 'date_to'=>$date_to ]) ){
            code::json_die(['status'=>'ER', 'line'=>__LINE__, 'msg'=>__FUNCTION__.':'.__LINE__.dbe()]);

        } else if(! $res = net::wjson(FCBT_NODE.'/api.php', array_merge(
                $bot_specs, 
                [ 
                    'exchange' => 'bybit',
                    'date_from'=>time::date_atomize($date_from),
                    'date_to'=>time::date_atomize($date_to)
                ]
            ), [], true) ){
            
            code::json_die(['status'=>'ER', 'line'=>__LINE__, 'msg'=>proc::error() ]);
            
            
        } else {

            extract($res);

            if(! dbq(" UPDATE `test` SET `path`='${path}' WHERE `id`=${test_id} AND `path` IS NULL LIMIT 1 ") ){
                code::json_die(['status'=>'ER', 'line'=>__LINE__, 'msg'=>__FUNCTION__.':'.__LINE__]);

            } else {
                code::json_die(['status'=>'OK', 'line'=>__LINE__, 'msg'=>'processing', 'path'=>$path]);
            }

        }

    }

}



function backtest_serv(){

    if(! $rs = dbq(" SELECT `id`, `path` FROM `test` WHERE `path`!='' AND `done`=0 ORDER BY `id` ASC ") ){
        echo dbe();

    } else if( dbn($rs) ){

        while( $rw = dbf($rs) ){
            
            extract($rw);
            $path_uri = FCBT_NODE."/data/${path}/";

            // if the request is not completed
            if(! $res = net::wjson("${path_uri}.json") ){
                // echo "#${id} ".__LINE__."<br>"."${path_uri}.json<br>";

            // if the backtest agent is running, but the prcess is stopped, mark it as failed. (could not put in the list in a proper way)
            } else if( net::wget($path_uri) and !net::wget("${path_uri}.post") ){
                // echo "#${id} ".__LINE__."<br>";
                dbq(" UPDATE `test` SET `done`=-1 WHERE `id`=${id} LIMIT 1 ");
                continue;
            
            } else {

                extract($res);

                $query = " UPDATE `test` SET 

                    `done`=1, 
                    `round`={$round}, 
                    `percent`={$percent}, 
                    `risk`='{$risk}'
                    ".( is_null($liq_price) ? '' : ", `liq_price`={$liq_price}, `liq_date`='{$liq_date}' " )."

                WHERE `id`={$id} LIMIT 1 ";

                if(! dbq($query) )
                    echo dbe();

            }

        }

    }

}


function backtest_log(){

    backtest_serv();

    if(! $test_id = $_GET['test_id'] ){
        echo "no test_id defined";

    } else if(! $rs = dbq(" SELECT `path` FROM `test` WHERE `id`={$test_id} AND `done`=1 LIMIT 1 ") ){
        echo __LINE__;
        
    } else if(! $rw = dbf($rs) ){
        echo "no such test process exists. "." SELECT `path` FROM `test` WHERE `id`={$test_id} AND `done`=1 LIMIT 1 ";

    } else {
        echo "<pre style=margin:20px>";
        echo file_get_contents(FCBT_NODE.'/data/'.$rw['path'].'/.log');
        echo "</pre>";
    }

}