<?php


function cron_main(){

    if( defined('DEBUG_MODE') )
        log_sys("DEBUG MODE");

    # get all active bots
    $rw_s = dbqf(" SELECT `id` FROM `bot` WHERE `status` NOT IN ('', 'stopped') ORDER BY `id` ASC ");
    
    if(! sizeof($rw_s) ){
        log_sys("no bot to start");
    
    # for each active bots
    } else foreach( $rw_s as $rw ){

        extract($rw);
        
        shell_exec(" cd /var/www; php cron.php :{$id}: >>/tmp/perp-fibo-trader/log/sys.txt 2>>/tmp/perp-fibo-trader/log/sys.txt & ");
        
        # if debug mode, exit at first
        if( defined('DEBUG_MODE') )
            die;

    }

}

