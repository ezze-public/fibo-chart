<?php


/**
 * v-0.1 2022-11-15
 */

class fibo_chart
{
	
    
    public static function handle(){
        
        switch( bot_rw(BOT_ID)['status'] ){

            case 'starting':
                return self::start_from_scratch();
    
            case 'started':
            case 'stopping':
            case 'safestopping':
                return self::cover_the_proc();

            case 'terminating':
                return self::terminate();

            case 'disconnecting':
                return self::disconnect();
        
        }

    }


    private static function terminate(){
        log_this('logg', __FUNCTION__.':'.__LINE__);

        // close orders and positions
        (EX)::close_all_orders(CREDENTIALS, SYMBOL);
        (EX)::close_all_positions(CREDENTIALS, SYMBOL);
        self::log_db("closed all orders and positions");
        
        // set total_profit on proc_rw
        self::profit_calculate( (EX)::price(CREDENTIALS, SYMBOL) );

        // pretend as disconnected
        self::disconnect();
        
    }
    

    public static function disconnect(){

        proc_rw([
            'liq_price' => null,
            'liq_margin' => null,
            'done' => 1
        ]);
        bot_rw(BOT_ID, [ 'status' => '' ]);

        self::log_db("FINN");
        die;

    }


	private static function start_from_scratch(){
        
        $leverage = bot_rw(BOT_ID, 'leverage');

        if(! $leverage = (EX)::set_leverage(CREDENTIALS, SYMBOL, $leverage, $leverage) ){
            self::log_db("CAN'T SET LEVERAGE OF ".SYMBOL." to {$leverage}");
            die;
        }

        if(! (EX)::set_dual(CREDENTIALS, false) ){
            self::log_db("CAN'T SET DUAL OF ".SYMBOL);
            die;
        }

        bot_rw(BOT_ID, [ 'status' => 'started' ]);
        self::build_new_proc_if_not();

    }


    private static function build_new_proc_if_not(){
        
        if( $rw = dbqf(" SELECT `id` FROM `proc` WHERE `done`=0 AND `bot`=".BOT_ID." LIMIT 1 ") ){
            $id = $rw['id'];
            self::log_db("OLD PROC $id");

        } else {
            $bot_rw = bot_rw(BOT_ID);
            $bot_specs = [
                'symbol' => $bot_rw['symbol'],
                'leverage' => $bot_rw['leverage'],
                'budget' => $bot_rw['budget_in_use'],
                'step_gap'=> $bot_rw['step_gap'],
                'max_step'=> $bot_rw['max_step'],
                'valid_step'=> $bot_rw['valid_step'],
            ];
            $id = dbin('proc', [ 'bot' => BOT_ID, 'bot_specs'=>json_encode($bot_specs), 'date_start' => time::date_in_iran("Y-m-d H:i:s") ]);
            self::log_db("NEW PROC $id");
        }

        define('PROC_ID', $id);

        log_this('logg', __FUNCTION__.':'.__LINE__);
        (EX)::close_all_orders(CREDENTIALS, SYMBOL);
        (EX)::close_all_positions(CREDENTIALS, SYMBOL);

    }


    private static function cover_the_proc(){

        if(! $proc_rw = proc_rw() ){
            log_this('logg', __FUNCTION__.':'.__LINE__);
            
            if( 0 /* if stopping or safestopping */ ){
                // just quit

            } else {
                log_this('logg', __FUNCTION__.':'.__LINE__);
                self::build_new_proc_if_not();
            }

        } else if(! sizeof( code::array_from_json($proc_rw['step.json']) ) ){
            log_this('logg', __FUNCTION__.':'.__LINE__);
            self::sync_step();

        } else if(! sizeof( code::array_from_json($proc_rw['trigger.json']) ) ){
            log_this('logg', __FUNCTION__.':'.__LINE__);
            self::sync_trigger();

        } else {
            log_this('logg', __FUNCTION__.':'.__LINE__);
            self::do_the_trade();
        }
        
    }


    private static function sync_step(){
        log_this('logg', __FUNCTION__.':'.__LINE__);

        $res = proc_rw([ 
            'trigger.json'    => '', 
            'curr_step'       => 0, 
            'date_round'      => time::date_in_iran("Y-m-d H:i:s"), 
            'liq_price'       => null,
            'liq_margin'      => null,
            'count_of_rounds' => proc_rw('count_of_rounds')+1
        ]);

        if(! $res ){
            log_this('logg', __FUNCTION__.':'.__LINE__);
            echo dbe();
            die;
        }

        $price = (EX)::price(CREDENTIALS, SYMBOL);
        if( $init_round_price = proc_rw('init_round_price') ){
            $price_origin = $price;
            if( $init_round_price > $price ){
                $price_one_perc = $price + ($price *0.011);
                $price = min($price_one_perc, $init_round_price);
            } else {
                $price_one_perc = $price - ($price *0.011);
                $price = max($price_one_perc, $init_round_price);
            }
            $price = (EX)::set_price(SYMBOL, $price);
        }

        $steps[ 0 ]['price'] = $price;
        $steps[ 0 ]['volume'] = 0;

        $price_up = $price;
        $price_dn = $price;

        $base_volume = self::set_base_volume($price);
        $init_round_wallet = proc_rw('init_round_wallet');
        self::log_db("START WITH $init_round_wallet ".BASE." @{$price}".($price_origin ? '->'.$price_origin:'') ." BASE_VOLUME {$base_volume} ".COIN);

        foreach( fibo_serial(MAX_STEP) as $step => $fibo ){
            
            $price_up = $price_up * (1 + ( $fibo * STEP_GAP / 100 ));
            $price_dn = $price_dn * (1 - ( $fibo * STEP_GAP / 100 ));

            $steps[     $step ] = [ 'price' => (EX)::set_price(SYMBOL, $price_up), 'volume' => (EX)::set_volume(SYMBOL,$base_volume * $fibo) ];
            $steps[ -1* $step ] = [ 'price' => (EX)::set_price(SYMBOL, $price_dn), 'volume' => (EX)::set_volume(SYMBOL,$base_volume * $fibo) ];

        }

        krsort($steps);
        proc_rw([ 'step.json' => json_encode($steps) ]);

    }


    private static function set_base_volume( $price ){
        log_this('logg', __FUNCTION__.':'.__LINE__);

        $init_round_wallet = number_format((EX)::balance(CREDENTIALS, BASE), 2, '.', '');
        $proc_rw = proc_rw([ 'init_round_wallet' => $init_round_wallet ]);
        if(! $proc_rw['init_boot_wallet'] )
            proc_rw([ 'init_boot_wallet' => $init_round_wallet ]);

        $wallet_x = $init_round_wallet * LEVERAGE * BUDGET_IN_USE; // available volume
        $wallet_x = floor($wallet_x); // round the USDT
        $volume = $wallet_x / $price; // make a division
        $portion = array_sum( fibo_serial(MAX_STEP) );
        $volume_portion = floor($wallet_x / $portion);
        $volume/= $portion; // grab the budget for step one, between the whole budget.
        $base_volume = (EX)::set_volume(SYMBOL, $volume); // cut the edge
        
        if(! $base_volume ){
            $volume_step = (EX)::exchangeInfo_pair(SYMBOL)['minQty'];
            $volume_step_base = ceil($volume_step * $price);
            self::log_db("BASE_VOLUME of $volume_portion ".BASE." is less than the threshold of $volume_step_base ".BASE, true);
            self::disconnect();
        }

        proc_rw([ 'base_volume' => $base_volume ]);

        return $base_volume;
        
    }


    private static function sync_trigger(){
        log_this('logg', __FUNCTION__.':'.__LINE__);

        $proc_rw = proc_rw();
        $curr_step = $proc_rw['curr_step'];        
        $step_s = code::array_from_json($proc_rw['step.json']);

        $step_up = $curr_step +1;
        $step_dn = $curr_step -1;

        $trigger_s = [];

        if( abs($step_up) <= VALID_STEP and array_key_exists($step_up, $step_s) ){
            $trigger_s['over']['step'] = $step_up;
            $trigger_s['over']['close'] = intval($curr_step < 0);
            $log.= ($trigger_s['over']['close'] ? "CLOSE":"SELL"). " @".$step_s[$step_up]['price'];
            if(! $trigger_s['over']['close'] )
                $log.= " v".$step_s[$step_up]['volume'];
        }

        $log.= $log ? ' or ' : '';

        if( abs($step_dn) <= VALID_STEP and array_key_exists($step_dn, $step_s) ){
            $trigger_s['undr']['step'] = $step_dn;
            $trigger_s['undr']['close'] = intval($curr_step > 0);
            $log.= ($trigger_s['undr']['close'] ? "CLOSE":"LONG"). " @".$step_s[$step_dn]['price'];
            if(! $trigger_s['undr']['close'] )
                $log.= " v".$step_s[$step_dn]['volume'];
        }

        self::log_db("TRIGGER {$log}");

        proc_rw([ 'trigger.json' => json_encode($trigger_s) ]);

    }


    private static function do_the_trade(){
        log_this('logg', __FUNCTION__.':'.__LINE__.' -- ');
        
        $pos_s = self::position_sync_sort();

        $bot_rw = bot_rw(BOT_ID);
        $proc_rw = proc_rw();
        $curr_step = $proc_rw['curr_step'];
        $trigger_s = code::array_from_json($proc_rw['trigger.json']);
        $step_s = code::array_from_json($proc_rw['step.json']);
        
        if( $if_undr = array_key_exists('undr', $trigger_s) ){
            log_this('logg', __FUNCTION__.':'.__LINE__);
            $step_dn = $trigger_s['undr']['step'];
            if(! $undr_is_close = $trigger_s['undr']['close'] )
                $undr_is_step = true;
        }

        if( $if_over = array_key_exists('over', $trigger_s) ){
            log_this('logg', __FUNCTION__.':'.__LINE__);
            $step_up = $trigger_s['over']['step'];
            if(! $over_is_close = $trigger_s['over']['close'] )
                $over_is_step = true;
        }

        # want to stop, and it just started, its OK to stop the process
        if( $bot_rw['status'] == 'stopping' and $curr_step == 0 and $pos_s['sell'] == 0 and $pos_s['long'] == 0 ){
            log_this('logg', __FUNCTION__.':'.__LINE__);
            (EX)::close_all_orders(CREDENTIALS, SYMBOL);
            self::return_spare_if_any();
            self::hook_controller('stopping');
        }
        
        # not first 'step', its 'close', empty 'pos'
        if(
            ( $curr_step > 0 and $undr_is_close and $pos_s['sell'] == 0 ) # /\  -- open sell, then close sell
            or
            ( $curr_step < 0 and $over_is_close and $pos_s['long'] == 0 ) # \/ -- open log, then close long
        ){
            log_this('logg', __FUNCTION__.':'.__LINE__);
            (EX)::close_all_orders(CREDENTIALS, SYMBOL);
            self::position_sync_sort(); // sync the liq_price
            $close_step = $trigger_s[ $curr_step > 0 ? 'undr' : 'over' ]['step'];
            $price = $step_s[ $close_step ]['price'];
            self::return_spare_if_any();
            self::profit_calculate($price);
            self::hook_controller('stopping');
            # next round
            proc_rw([ 'init_round_price' => $step_s[ $curr_step ]['price'] ]);
            self::sync_step();
        
        # not 'undr', not 'close', 'pos[sell]' is full
        } else if( $curr_step >= 0 and $over_is_step and $pos_s['sell'] >= self::total_of_volume_til_step($step_up) ){
            log_this('logg', __FUNCTION__.':'.__LINE__.', $pos_s[sell] is '.$pos_s['sell'].', total_of_volume_til_step is '.self::total_of_volume_til_step($step_up) );
            (EX)::close_all_orders(CREDENTIALS, SYMBOL);
            self::position_sync_sort(); // sync the liq_price
            self::go_next_step('up');
            self::log_db("FILL OPEN-SELL lq: ".proc_rw('liq_price') );
            self::sync_trigger();

        # not 'over', not 'close', 'pos[long]' is full
        } else if( $curr_step <= 0 and $undr_is_step and $pos_s['long'] >= self::total_of_volume_til_step($step_dn) ) {
            log_this('logg', __FUNCTION__.':'.__LINE__.', $pos_s[long] is '.$pos_s['long'].', total_of_volume_til_step is '.self::total_of_volume_til_step($step_dn) );
            (EX)::close_all_orders(CREDENTIALS, SYMBOL);
            self::position_sync_sort(); // sync the liq_price
            self::go_next_step('down');
            self::log_db("FILL OPEN-LONG lq: ".proc_rw('liq_price') );
            self::sync_trigger();
        
        # not enough 'pos'
        } else {

            if( $if_undr ){
                log_this('logg', __FUNCTION__.':'.__LINE__);
                if( $undr_is_close ){ # CLOSE SELL => /\
                    log_this('logg', __FUNCTION__.':'.__LINE__);
                    $volume = $pos_s['sell'];
                    $price = $step_s[ $step_dn ]['price'];
                    // self::log_db("** pos_s[sell] ".$pos_s['sell']);

                    $dimention = 'close-sell';
                    if( $res = self::order_volume_needed_if_any($dimention, $volume) ){
                        log_this('logg', __FUNCTION__.':'.__LINE__);
                        extract($res);
                        $curr_orders = self::sum_of_order_volume($dimention);
                        (EX)::order(CREDENTIALS, 'LIMIT', $dimention, SYMBOL, $volume_needed, $price);
                        self::log_db("OPEN ".strtoupper($dimention)." @{$price} v{$volume_needed}/{$volume}".( $curr_orders ? ' RE' : '' ) );
                    }

                } else { # OPEN LONG => \
                    log_this('logg', __FUNCTION__.':'.__LINE__);
                    if( !$bot_rw['valid_step'] or abs($step_dn) <= $bot_rw['valid_step'] ){
                        $volume = self::total_of_volume_til_step($step_dn) - $pos_s['long'];
                        $price = $step_s[ $step_dn ]['price'];

                        $dimention = 'open-long';
                        if( $res = self::order_volume_needed_if_any($dimention, $volume) ){
                            log_this('logg', __FUNCTION__.':'.__LINE__);
                            extract($res);
                            sleep(1);
                            $pos_s = self::position_sync_sort();
                            if( $curr_step <= 0 and $undr_is_step and $pos_s['long'] >= self::total_of_volume_til_step($step_dn) ) {
                                log_this('logg', __FUNCTION__.':'.__LINE__);
                                // order is filled but no body knows. ignore this OPEN and let it have a loop.
                                return;

                            } else {
                                log_this('logg', __FUNCTION__.':'.__LINE__);
                                $curr_orders = self::sum_of_order_volume($dimention);

                                if( $volume_needed >= (EX)::min_volume(SYMBOL) ){
                                    (EX)::order(CREDENTIALS, 'LIMIT', $dimention, SYMBOL, $volume_needed, $price, ['RE']);
                                    self::log_db("OPEN ".strtoupper($dimention)." @{$price} v{$volume_needed}/{$volume}".( $curr_orders ? ' RE' : '' ) );
                                
                                } else {
                                    self::log_db("OPEN ".strtoupper($dimention)." @{$price} v{$volume_needed}/{$volume}".( $curr_orders ? ' RE ignored' : '' ) );
                                }

                            }
                        
                        } else {
                            log_this('logg', __FUNCTION__.':'.__LINE__);
                        }

                    }
                }

            }
            
            if( $if_over ){
                log_this('logg', __FUNCTION__.':'.__LINE__);
                if( $over_is_close ){ # CLOSE LONG => \/
                    log_this('logg', __FUNCTION__.':'.__LINE__);
                    $volume = $pos_s['long'];
                    $price = $step_s[ $step_up ]['price'];
                    // self::log_db("** pos_s[long] ".$pos_s['long']);

                    $dimention = 'close-long';
                    if( $res = self::order_volume_needed_if_any($dimention, $volume) ){
                        log_this('logg', __FUNCTION__.':'.__LINE__);
                        extract($res);
                        $curr_orders = self::sum_of_order_volume($dimention);
                        (EX)::order(CREDENTIALS, 'LIMIT', $dimention, SYMBOL, $volume_needed, $price);
                        self::log_db("OPEN ".strtoupper($dimention)." @{$price} v{$volume_needed}/{$volume}".( $curr_orders ? ' RE' : '' ) );
                    }

                } else { # OPEN SELL => /
                    log_this('logg', __FUNCTION__.':'.__LINE__);
                    if( !$bot_rw['valid_step'] or abs($step_up) <= $bot_rw['valid_step'] ){
                        log_this('logg', __FUNCTION__.':'.__LINE__);
                        $volume = self::total_of_volume_til_step($step_up) - $pos_s['sell'];
                        $price = $step_s[ $step_up ]['price'];

                        $dimention = 'open-sell';
                        if( $res = self::order_volume_needed_if_any($dimention, $volume) ){
                            log_this('logg', __FUNCTION__.':'.__LINE__);
                            extract($res);
                            sleep(1);
                            $pos_s = self::position_sync_sort();
                            if( $curr_step >= 0 and $over_is_step and $pos_s['sell'] >= self::total_of_volume_til_step($step_up) ){
                                log_this('logg', __FUNCTION__.':'.__LINE__);
                                // order is filled but no body knows. ignore this OPEN and let it have a loop.
                                return;

                            } else {
                                log_this('logg', __FUNCTION__.':'.__LINE__);
                                $curr_orders = self::sum_of_order_volume($dimention);

                                if( $volume_needed >= (EX)::min_volume(SYMBOL) ){
                                    (EX)::order(CREDENTIALS, 'LIMIT', $dimention, SYMBOL, $volume_needed, $price);
                                    self::log_db("OPEN ".strtoupper($dimention)." @{$price} v{$volume_needed}/{$volume}".( $curr_orders ? ' RE' : '' ) );
                                
                                } else {
                                    self::log_db("OPEN ".strtoupper($dimention)." @{$price} v{$volume_needed}/{$volume}".( $curr_orders ? ' RE ignored' : ' ignored' ) );
                                }

                            }
                        
                        } else {
                            log_this('logg', __FUNCTION__.':'.__LINE__);  
                        }

                    }
                }
            
            }

            self::sync_liq_margin();

            log_sys('.');
            sleep(4);
            
        }
        
    }


    private static function sync_liq_margin(){
        
        $rw = proc_rw();
        $liq_price = $rw['liq_price'];
        $liq_margin = $rw['liq_margin'];

        $price = (EX)::price(CREDENTIALS, SYMBOL);

        $new_margin = 
            round( 
                abs( $price - $liq_price )
                * 1000
                / $price
            )
            / 10;
        
        $new_margin = $new_margin > 98 ? 100 : $new_margin;

        if( is_null($liq_margin) or $new_margin < $liq_margin )
            proc_rw([ 'liq_margin' => $new_margin ]);
        
    }


    private static function sync_liq_price( $pos ){
        if( $pos['position'] )
            proc_rw(['liq_price' => $pos['liq_price'] ]);    
    }
    

    private static function order_volume_needed_if_any( $dimention, $volume_needed ){
        
        $curr_orders = self::sum_of_order_volume($dimention);
        $volume_needed = wash_my_float($volume_needed) - wash_my_float($curr_orders);

        if( $volume_needed ){
            log_this('logg', 'sum_of_order_volume => curr_orders: '.$curr_orders.', volume_needed: '.$volume_needed);
            $volume_needed = (EX)::set_volume(SYMBOL, $volume_needed);
            return [ 'volume_needed'=>$volume_needed, 'curr_orders'=>$curr_orders ];
        }

        return false;

    }


    private static function sum_of_order_volume( $dimention ){

        $order_s = (EX)::order_list(CREDENTIALS, SYMBOL, $dimention);
        $sum = 0;

        foreach( $order_s as $o )
            $sum+= $o['origQty'] - $o['executedQty'];
        
        return wash_my_float($sum);

    }


    private static function total_of_volume_til_step( $step ){

        $step_s = code::array_from_json( proc_rw('step.json') );
        $volume = 0;

        if( $step > 0 ){
            for( $i=0; $i<= $step; $i++ ){
                $volume += $step_s[ $i ]['volume'];
            }
        
        } else if( $step < 0 ){
            for( $i=0; $i>= $step; $i-- ){
                $volume += $step_s[ $i ]['volume'];
            }
        }

        return wash_my_float($volume);

    }


    private static function go_next_step( $the_way ){
        log_this('logg', __FUNCTION__.':'.__LINE__);

        $curr_step = proc_rw('curr_step');

        switch( $the_way ){
            case 'up':   if($curr_step >= 0) $curr_step++; break;
            case 'down': if($curr_step <= 0) $curr_step--; break;
        }
        
        proc_rw([ 'curr_step' => $curr_step ]);

    }


    private static function hook_controller( $hook ){
        // log_sys(__FUNCTION__.":{$hook}");
        
        switch( $hook ){
            case 'stopping':
                $bot_rw = bot_rw(BOT_ID);
                if( $bot_rw['status'] == 'stopping' )
                    self::disconnect();
                break;
        }
    }

    
    // order_id => filled, amount
    private static function order_sort( $order_s ){
        
        if(! sizeof($order_s) ){
            $sort = [];

        } else {
            foreach( $order_s as $o ){
                $sort[ $o['order_id'] ] = [
                    'filled' => $o['cum_exec_qty'],
                    'amount' => $o['qty']
                ];
            }
        }

        return $sort;

    }


    private static function position_sync_sort(){
        
        $pos = (EX)::position_list(CREDENTIALS, SYMBOL);
        self::sync_liq_price($pos);
        
        return [ 
            'long' => ($pos['position'] == 'long') ? $pos['amount'] : 0 ,
            'sell' => ($pos['position'] == 'sell') ? $pos['amount'] : 0 ,
        ];

    }

    
    private static function profit_calculate( $close_price ){
        
        $bot_rw = bot_rw(BOT_ID);
        $proc_rw = proc_rw();

        $init_boot_wallet = $proc_rw['init_boot_wallet'];
        $init_round_wallet = $proc_rw['init_round_wallet'];
        $curr_wallet = (EX)::balance(CREDENTIALS, BASE);

        if(! $init_boot_wallet )
            return self::log_db("CLOSE WITH $curr_wallet ".BASE." @{$close_price} no profit ");
        
        $total_profit = round($curr_wallet - $init_boot_wallet, 2);
        $total_percent = round( $total_profit * 100 / $init_boot_wallet, 2);
        proc_rw([ 'total_profit' => $total_profit, 'total_percent' => $total_percent ]);
        $total_time = time::sec_to_hour_n_min( time::date_diff_now($proc_rw['date_start']) );

        $round_profit_raw = $curr_wallet - $init_round_wallet;
        $round_profit = round($round_profit_raw, 2);
        $round_percent = round( $round_profit * 100 / $init_round_wallet, 2);
        $round_time = time::sec_to_hour_n_min( time::date_diff_now($proc_rw['date_round']) );
        
        if( $round_percent < -10 ){
            self::log_db("LIQUIDATED.");
            self::disconnect();
        }

        if( $save_perc = $bot_rw['save_perc'] ){
            
            if( $save_perc == 100 ){
                $move = $round_profit_raw;
                
            } else {
                $move = round( $round_profit_raw * ($save_perc / 100) , 2 );
            }
            
            // if( $move > 0 ){
                // (EX)::transfer(CREDENTIALS, BASE, $move, 'spot');
            //     $curr_wallet-= $move;
            //     $move_log = " [".round($move,2)." ".BASE." moved]";
            // }
            
        }

        $curr_wallet = round($curr_wallet, 2);

        $liq_price = $proc_rw['liq_price'];
        $liq_margin = $proc_rw['liq_margin'];
        $liq_margin = $liq_margin == 100
            ? '∞'
            : $liq_margin.'%';

        // log some where the round profit
        self::log_db("CLOSE WITH {$curr_wallet} ".BASE." @{$close_price} {$round_profit} {$round_percent}% in {$round_time}".
            " total {$total_profit} {$total_percent}% in {$total_time} lq {$liq_price} lm {$liq_margin} {$move_log}");
        
    }


    private static $side_s = [
        'over' => 'sell',
        'undr' => 'long',
    ];


    private static function side_by_way( $way ){
        return self::$side_s[$way];
    }


    private static function side_reverse( $side ){
        $flipped = array_flip(self::$side_s);
        return self::$side_s[ ($flipped[$side] + 1)%2 ];
    }

    
    private static function log_db( $text, $stop_the_bot = false ){
        
        if( substr($text, 0, 6) == 'CLOSE ' or substr($text, 0, 5) == 'FILL ' )
            $need_blank_after = true;

        if( substr($text, 0, 6) == 'START ' )
            $need_blank_before = true;

        if( defined('BOT_ID') and defined('PROC_ID') ){
            
            log_this('db', $text);
            log_this('logg', $text);

            $curr_step = proc_rw('curr_step');
            $text = ( $curr_step == 0 ? ' ' : ($curr_step>0?'+':'') ).$curr_step." ".$text;

            // if( $need_blank_before )
            //     log_this('db', '{'.BOT_ID.'}');
            // log_this('db', '{'.BOT_ID.'} '.$text);
            // if( $need_blank_after )
            //     log_this('db', '{'.BOT_ID.'}');
            
            $text = time::date_in_iran('Y-m-d H:i:s')." ".$text;

            if( $stop_the_bot )
                bot_rw(BOT_ID, [ 'status' => '' ]);

            return dbin('log', [ 'bot' => BOT_ID, 'proc' => PROC_ID, 'text' => $text ]);

        }

        return false;

    }


    private static function return_spare_if_any(){

        extract(bot_rw(BOT_ID));

        // if( $spare )
        //     if( (EX)::transfer(CREDENTIALS, BASE, $spare, 'spot') )
        //         bot_rw(BOT_ID, ['spare'=>0]);

    }


}


