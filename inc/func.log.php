<?php


function log_sys( $text ){
    
    if( defined('BOT_ID') )
        $text = '[b'.BOT_ID.'] '.$text;

    return log_this('sys', $text);
}


function log_this( $type, $text ){

    if( $text === null ){
        $text = "";
        
    } else if( $text === '' ){
        $text = '[empty request]';
        
    } else if( is_array($text) or is_object($text) ){
        $text = json_encode($text);
    }
    
    $date = time::date_in_iran("d M H:i:s");
    $text = "{$date} {$text}";

    $file = "/tmp/perp-fibo-trader/log/{$type}.txt";

    return put_in_log_file($file, $text);

}


function put_in_log_file( $file, $text ){
    
    sys::take_care_of_file($file);

    $fp = fopen($file, 'a+');
    $text.= "\n";

    fwrite($fp, $text);
    fclose($fp);

    return $text;

}

