<?php


function botlist(){

    if( $user_id = login_id() ){

        if( isset($_GET['do2']) ){
            switch($_GET['do2']){
                
                case 'form':
                    return botlist_form();
                
                case 'saveNew':
                    botlist_saveNew();
                    break;

                case 'saveEdit':
                    botlist_saveEdit();
                    break;
        
                case 'backtest':
                    return backtest_list();
    
                case 'backtest_log':
                    return backtest_log();
                        
            }
        }

        echo "<div class=\"botlist\" >"; // do-ajax=\"./?do_action=sync-list\" interval=\"4\"

        echo "
        <div class=\"re h\" >
            <div class=\"name\">bot name</div>
            <div class=\"symbol\">symbol</div>
            <div class=\"stat\"></div>
            <div class=\"tools\"></div>
        </div>";
        
        if(! sizeof($rw_s = dbqf(" SELECT * FROM `bot` WHERE `api` IN (SELECT `id` FROM `api` WHERE `user`=$user_id) ORDER BY `id` ASC ")) ){
            echo "<br><br><br><br><br><div style=text-align:center >click <a href=\"./?do=api\">here</a> to create your first bot.</div><br><br><br><br><br>";

        } else foreach( $rw_s as $rw ){
            
            extract($rw);

            $api_rw = api_rw($rw['api']);
            $EX = $api_rw['api_ex'];
            
            $credentials = $api_rw['api_key'].':'.$api_rw['api_secret'];

            if(! $max_step )
                $max_step = 6;

            // if( strlen($budget_in_use) == 3 )
            //     $budget_in_use.= '0';

            // $liq_price = 0;
            // if( $proc = botlist_latest_proc($id) ){
            //     $liq_price = proc_rw('liq_price', $proc);
            // }

            echo "
            <div class=\"re r".( $balance == 'N/A' ? ' na' : '' )."\" botlist_string=\"no-more\" bot=\"{$id}\" >
                
                <div class=\"name\">{$bot_name}</div>
                <div class=\"symbol\">{$symbol}</div>

                <div class=\"stat\"></div>

                <div class=\"tools\">
                
                    <a class=\"".( $wallet_address ? 'copyToClip ':'disabled ')."qrcode\" ".($wallet_address ? 'code="'.$wallet_address.'" ':'')."title=\"wallet qrcode\" href=\"#\"><i class=\"fa-solid fa-qrcode\"></i></a>

                    <a class=\"disabled start\" title=\"start\" do-ajax=\"./?do_action=sync-bot-params&param=status&value=starting&id={$id}\"><i class=\"fa-solid fa-play\"></i></a>
                    <a class=\"disabled stop\" title=\"stop\" do-ajax=\"./?do_action=sync-bot-params&param=status&value=stopping&id={$id}\"><i class=\"fa-solid fa-stop\"></i></a>
                    <!--<a class=\"disabled safestop\" title=\"safe stop\" do-ajax=\"./?do_action=sync-bot-params&param=status&value=safestoping&id={$id}\"><i class=\"fa-regular fa-life-ring\"></i></a>-->
                    <a class=\"disabled terminate\" title=\"terminate\" do-ajax=\"./?do_action=sync-bot-params&param=status&value=terminating&id={$id}\" confirm=\"1\" ><i class=\"fa-solid fa-scissors\"></i></a>
                    <a class=\"disabled disconnect\" title=\"disconnect\" do-ajax=\"./?do_action=sync-bot-params&param=status&value=disconnecting&id={$id}\" confirm=\"1\" confirm=\"1\" ><i class=\"fa-solid fa-link-slash\"></i></a>

                    <a class=\"disabled remove\" title=\"remove\" do-ajax=\"./?do_action=bot-remove&id={$id}\" confirm=\"1\"><i class=\"fa-solid fa-trash-can\"></i></a>
                    <a class=\"modify\" title=\"modify\" href=\"./?do=bot&do2=form&id={$id}\"><i class=\"fa-solid fa-pen-to-square\"></i></a>
                    <!-- <a class=\"backtest\" title=\"list of backtests\" href=\"./?do=bot&do2=backtest&id={$id}\" ><i class=\"fa-solid fa-vial\"></i></a> -->
                    
                    <a title=\"log\" class=\"log_a\" href=\"./?do_action_pre=log&bot={$id}&proc=latest\" target=\"_blank\"><i class=\"fa-solid fa-file\"></i></a>
                    <a title=\"log list\" href=\"./?do_action_pre=log&bot={$id}\" target=\"_blank\"><i class=\"fa-solid fa-list-ul text-sky-500\"></i></a>
                </div>

            </div>";

        }

        ?>
        <!-- <div class="re t" >
            <div class="name"></div>
            <div class="symbol"></div>
            <div class="stat"></div>
            <div class="tools">
                <a title="add" href="./?do=api"><i class="fa-solid fa-square-plus text-blue-500"></i></a>
            </div>
        </div> -->
        </div>
        <script src="./lib/tail.botlist.js"></script>
        <?php
    
    }

}


function botlist_form(){

    if(! $user_id = login_id() ){
        return false;

    } else if( $id = intval($_GET['id']) and $rw = bot_rw($id) ){
        extract($rw);

        $max_step = $max_step ? : 6;
        $rest_of_path = "Edit&id=${id}";

        $stopped = in_array($status, ['', 'stopped']);

    } else if( $api = $_GET['api'] ) {
        $rest_of_path = "New&api=${api}";

    } else {
        die('no api defined');
    }

    $api_rw = api_rw($api);

    $date_to = date('Y-m-d', strtotime(date('Y-m-d').' -1 day'));
    $date_from = date('Y-m-d', strtotime($date_to.' -1 month'));

    ?>
    <form name="form" method="post" action="./?do=bot&do2=save<?php echo $rest_of_path ?>" <?php echo ( $id ? 'bot_id="'.$id.'"' : '') ?> class="botlist_newForm">
        
        <hr>

        <div>
            <span>api</span>
            <input type="text" value="<?php echo $api_rw['api_name'] ?>" disabled/>
        </div>

        <label>
            <span>bot name</span>
            <input type="text" name="bot_name" value="<?php echo $rw['bot_name'] ?>" required="1"/>
            <memo>any thing. its just a name.</memo>
        </label>

        <hr>

        <div>
            <span>symbol</span>
            <item><input <?=( ($rw and !$stopped)?'readonly=1 class=disabled':'')?> type="text" name="symbol" value="<?php echo ( $symbol ? : 'BTC_USDT' ) ?>" autocomplete=off dropsearch="./?do_action=symbolselect-feed&ex=<?php echo $api_rw['api_ex'] ?>" required="1"/></item>
        </div>

        <label>
            <span>leverage</span>
            <input type="number" name="leverage" min="1" max="200" step="1" value="<?php echo ($rw ? $leverage : '100') ?>" required="1"/>
        </label>

        <label>
            <span>max step</span>
            <input type="number" name="max_step" min="4" max="10" step="1" value="<?php echo ($max_step ?: 6) ?>" required="1"/>
            <memo>1, 1, 2, 3, 5, 8 : 20</memo>
        </label>

        <label>
            <span>valid step</span>
            <input type="number" name="valid_step" step="1" max=10 min=2 value="<?php echo ($valid_step ? : ($max_step?: 6) ) ?>" required="1"/>
        </label>

        <label>
            <span>total budget</span>
            <input type="number" name="budget_in_use" min="5" max="70" step="1" value="<?php echo ($rw ? $budget_in_use * 100 : '5') ?>" required="1"/>
            <kelvin>
                <bowl>start with</bowl>
                <input type="number" name="first_step" value="" max="3.5" step="0.01" required="1"/> <!-- min="0.25" -->
                %&nbsp;
            </kelvin>
        </label>

        <label>
            <!-- <memo>[total budget]: the whole money we plan to put into the trade "in percent". [start with]: the amount we plan to use for the first step "in percent".</memo> -->
        </label>

        <label>
            <span>step gap</span>
            <input type="number" name="step_gap" min="0.3" max="2" step="0.1" value="<?php echo ($rw ? $step_gap : '1') ?>"/>
            <memo>the gap between step 0 and step 1 in percent. 1,1,2,3,5,8% for 1 or maybe 2,2,4,6,10,16% if you choose 2.</memo>
        </label>

        <!--
        <hr>

        <label>
            <span>liq margin</span>
            <?php echo botlist_template( 'liq_margin', $liq_margin, '#%') ?>
            <memo>if the liquidation price pass this margin and come closer, it will ignore the new orders.</memo>
        </label>

        <label style="display: none;">
            <span>force delay</span>
            <input required="1" name="force_delay" value="<?php echo ($force_delay ? : 0)?>" type="number" min="0" max="400" step="10" />
            <memo>delay after closing positions in force-exit mode</memo>
        </label>


        <label>
            <span>save perc</span>
            <?php echo botlist_template( 'save_perc', $save_perc, '#%') ?>
            <memo>portion of profit that will be transferred to spot balance.</memo>
        </label>
        -->
        
        <hr>

        <div>
            <span></span>
            <input type="submit" value="save"/>
            <?php if( $id ){ ?>
            or
            <span class="timerange">
                backtest: 
                <span class="the_form">
                    <select name="date_style">
                        <option value="6h">last 6 hours</option>
                        <option value="24h">last 24 hours</option>
                        <option value="2d">last 2 days</option>
                        <option value="7d">last 7 days</option>
                        <option value="30d">last 30 days</option>
                        <option value="90d">last 90 days</option>
                        <option value="365d">last year</option>
                        <option value="c">custom range</option>
                    </select>
                    <span class="customrange_form">
                        <?php $date_lvlb = gmdate('Y-m-d', gmdate('U')-3600*24).'T23:59:00'; ?>
                        <input type="datetime-local" max="<?php echo $date_lvlb ?>" value="<?php echo $date_from ?>T00:00" name="date_from"/>
                        <input type="datetime-local" max="<?php echo $date_lvlb ?>" value="<?php echo $date_to ?>T23:59" name="date_to"/>
                    </span>
                    <input type="button" value="proceed"/>
                    <a target="_blank" class="backtest" title="list of backtests" href="./?do=bot&do2=backtest&id=<?php echo $id ?>" ><i class="fa-solid fa-vials old" style="font-size:18px; position: relative; top: 3px;"></i></a>
                </span>
                <span class="the_messagebox">loading ..</span>
            </span>
            <?php } ?>
        </div>

    </form>
    <script src="./lib/tail.botlist_form.js"></script>
    <?php

}


function botlist_template( $name, $value, $pattern ){
    
    switch( $name ){
        case 'liq_margin' : $range = range(5,40, 5); break;
        case 'save_perc'  : $range = range(10,100, 10); break;
    }

    $id = isset($args[0]) ? $args[0] : null;
    $code = "<select name=\"{$name}\"><option value=\"0\">off</option>";
    foreach( $range as $k => $i )
        $code.= "<option value=\"{$i}\" ".($value==$i?'selected="1"':'').">".str_replace('#', $i, $pattern)."</option>";
    $code.= "</select>";

    return $code;
}


function botlist_saveNew(){
    
    if(! $user = login_id() ){
        echo "not logged in";

    } else if(! $api = intval($_GET['api']) ){
        echo "no api defined";

    } else if(! $api_rw = api_rw($api) ){
        echo "cant find relevant api";

    } else if(! class_exists($ex = $api_rw['api_ex']) ){
        echo "cant find relative class";

    } else if(! $bot_name = trim($_POST['bot_name']) ){
        echo "no bot name defined";

    } else if(! $symbol = strtoupper(trim($_POST['symbol'])) ){
        echo "no symbol defined";
    
    } else if(! $possible_max_leverage = ($ex)::leverageBracket_pair( $api_rw['api_key'].':'.$api_rw['api_secret'], $symbol) ){
        echo "cant load possible_max_leverage from {$ex}";
    
    } else if(! $leverage = min( intval($_POST['leverage']), $possible_max_leverage ) ){
        echo "no leverage defined";
    
    } else if( !is_numeric($leverage) or $leverage < 1 ){
        echo "wrong leverage defined";

    } else if( !$max_step = intval($_POST['max_step']) or $max_step < 4 or $max_step > 10 ){
        echo "wrong max step defined (don't be a mess)";

    } else if(! $budget_in_use = $_POST['budget_in_use'] / 100 ){
        echo "no budget defined";
    
    } else if( !is_numeric($budget_in_use) or $budget_in_use < 0.0001 or $budget_in_use > 0.70 ){
        echo "wrong budget defined";

    } else {

        $liq_margin = intval($_POST['liq_margin']);
        $force_delay= intval($_POST['force_delay']);
        $step_gap   = floatval($_POST['step_gap']);
        $save_perc  = intval($_POST['save_perc']);
        $valid_step  = intval($_POST['valid_step']);
        $wallet_address  = trim($_POST['wallet_address']);

        $query = " INSERT INTO `bot` (`api`, `bot_name`, `symbol`, `leverage`, `budget_in_use`, `liq_margin`, `force_delay`, `step_gap`, `save_perc`, `max_step`, `valid_step`) 
        VALUES ($api, '$bot_name', '$symbol', $leverage, $budget_in_use, $liq_margin, $force_delay, $step_gap, $save_perc, $max_step, $valid_step) ";

        if(! dbq($query) ){
            echo dbe();
            echo $query;

        } else {
            do_redirect('./?do=bot');
        }

    }

}


function botlist_saveEdit(){
    
    if(! $user_id = login_id() ){
        echo "not logged in";

    } else if(! $id = intval($_GET['id']) ){
        echo "no bot id defined";

    } else if(! $bot_rw = bot_rw($id) ){
        echo "cant find relevant bot";

    } else if(! $api_rw = api_rw($api = $bot_rw['api']) ){
        echo "cant find relevant api";

    } else if(! class_exists($ex = $api_rw['api_ex']) ){
        echo "cant find relative class";

    } else if(! $bot_name = trim($_POST['bot_name']) ){
        echo "no bot name defined";

    } else if(! $symbol = strtoupper(trim($_POST['symbol'])) ){
        echo "no symbol defined";
    
    } else if(! $possible_max_leverage = ($ex)::leverageBracket_pair( $api_rw['api_key'].':'.$api_rw['api_secret'], $symbol) ){
        echo "cant load possible_max_leverage from {$ex}";
    
    } else if(! $leverage = min( intval($_POST['leverage']), $possible_max_leverage ) ){
        echo "no leverage defined";
    
    } else if( !is_numeric($leverage) or $leverage < 1 ){
        echo "wrong leverage defined";

    } else if( !$max_step = intval($_POST['max_step']) or $max_step < 4 or $max_step > 10 ){
        echo "wrong max step defined (don't be a mess)";

    } else if(! $budget_in_use = $_POST['budget_in_use'] / 100 ){
        echo "no budget defined";
    
    } else if( !is_numeric($budget_in_use) or $budget_in_use < 0.0001 or $budget_in_use > 0.70 ){
        echo "wrong budget defined";

    } else {

        $liq_margin = intval($_POST['liq_margin']);
        $force_delay= intval($_POST['force_delay']);
        $step_gap   = floatval($_POST['step_gap']);
        $save_perc  = intval($_POST['save_perc']);
        $valid_step  = intval($_POST['valid_step']);
        $wallet_address  = trim($_POST['wallet_address']);

        $query = " UPDATE `bot`
            SET 
                `api`=$api,
                `bot_name`='$bot_name',
                `symbol`='$symbol', 
                `leverage`=$leverage, 
                `budget_in_use`=$budget_in_use, 
                `liq_margin`=$liq_margin, 
                `force_delay`=$force_delay, 
                `step_gap`=$step_gap, 
                `save_perc`=$save_perc, 
                `max_step`=$max_step, 
                `valid_step`=$valid_step
            WHERE 
                `id`=$id AND `api` IN (SELECT `id` FROM `api` WHERE `user`=$user_id)
            LIMIT 1 ";
        
        if(! dbq($query) ){
            echo dbe();
            echo $query;

        } else {
            do_redirect('./?do=bot');
        }

    }

}



