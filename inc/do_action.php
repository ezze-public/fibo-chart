<?php


function do_action_pre(){

    if( array_key_exists('do_action_pre', $_REQUEST) ){
        
        switch( $_REQUEST['do_action_pre'] ){

            case 'test':
                binance::test();
                die;

            case 'logout':
                login_exit();
                die;

            case 'log':
                botlist_log();
                die;

            case 'sync_bar_price':
                $res['status'] = 'ER';
                if( $proc = intval($_GET['proc']) ){
                    
                    extract(dbqf(" SELECT `bot`, `init_boot_wallet`, `init_round_wallet` FROM `proc` WHERE `id`=$proc LIMIT 1 "));
                    extract(dbqf(" SELECT `api`.`api_ex`, `api`.`api_key`, `api`.`api_secret`, `bot`.`symbol` FROM `bot` LEFT JOIN `api` ON `api`.`id`=`bot`.`api` WHERE `bot`.`id`=$bot LIMIT 1 "));
                    
                    $credentials = $api_key.":".$api_secret;
                    $res['price'] = $api_ex::price($credentials, $symbol);
                    $curr_wallet = $api_ex::balance($credentials, explode('_', $symbol)[1]);

                    $round_profit = $curr_wallet - $init_round_wallet;
                    $round_prefix = ($roudn_profit > 0 ? '+' : '');
                    $res['round_profit'] = $round_prefix . strval(number_format($round_profit, 2, '.', ''));
                    $res['round_percent'] = $round_prefix . strval(number_format( $res['round_profit'] * 100 / $init_round_wallet, 2, '.', ''));
                    $res['round_class'] = $round_profit > 0 ? 'green' : 'red';

                    $total_profit = $curr_wallet - $init_boot_wallet;
                    $total_prefix = ($total_profit > 0 ? '+' : '');
                    $res['total_profit'] = $total_prefix . strval(number_format($total_profit, 2, '.', ''));
                    $res['total_percent'] = $total_prefix . strval(number_format( $res['total_profit'] * 100 / $init_boot_wallet, 2, '.', ''));
                    $res['total_class'] = $total_profit > 0 ? 'green' : 'red';

                    $res['status'] = 'OK';

                }
                code::json_die($res);

            case 'sync_bar_etc':
                $res['status'] = 'ER';
                if( $proc = intval($_GET['proc']) ){
                    
                    extract(dbqf(" SELECT `bot`, `trigger.json` tj, `step.json` sj, liq_price FROM `proc` WHERE `id`=$proc LIMIT 1 "));
                    // extract(dbqf(" SELECT `api_key`, `api_secret`, `symbol` FROM `bot` WHERE `id`=$bot LIMIT 1 "));

                    if( $tj and $sj ){

                        $tj = json_decode($tj, true);
                        $sj = json_decode($sj, true);

                        $res['down'] = array_key_exists('undr', $tj) ? $sj[ $tj['undr']['step'] ]['price'] : 0;
                        $res['up'  ] = array_key_exists('over', $tj) ? $sj[ $tj['over']['step'] ]['price'] : 0;

                        $res['liq'] = $liq_price;

                        if(! $res['down'] )
                            $res['down'] = str_repeat('-', strlen($res['up']) );

                        if(! $res['up'] )
                            $res['up'] = str_repeat('-', strlen($res['down']) );

                        $res['status'] = 'OK';

                    }

                }
                code::json_die($res);

            case 'reset_proc_ptr':
                if( $proc = intval($_GET['proc']) and $proc_ptr = intval($_GET['proc_ptr']) ){
                    
                    dbq(" UPDATE `log` SET `seen`=1 WHERE `proc`=$proc ");

                    $rw_s = dbqf(" SELECT `id`, `text` FROM `log` WHERE `proc`=$proc AND `id` > $proc_ptr ORDER BY `id` ASC ", false, 'id>text');
                    
                    $res['status'] = 'OK';
                    
                    if( sizeof($rw_s) ){
                        $res['text'] = implode("\n", array_values($rw_s));
                        $res['proc_ptr'] = end(array_keys($rw_s));
                        $res['proc_ptr_i'] = intval($_GET['proc_ptr_i']);
                    }

                    
                } else {
                    $res['status'] = 'ER';
                    $res['code'] = 'no proc or proc_ptr defined';
                }
                code::json_die($res);

            case 'cmh_available':
                
                if(! $symbol = $_GET['symbol'] or !$tf = $_GET['tf'] ){
                    code::json_die([ 'status'=>'problem', 'msg'=>'no symbol or tf defined' ]);

                } else if(! $res = net::wjson(CMH_NODE."/available/binance/futures/{$symbol}/{$tf}") ){
                    // code::json_die([ 'status'=>'ER', 'msg'=>proc::error() ]);

                    // make new need request
                    $res = net::wjson(CMH_NODE.'/need/binance/futures/ATOM_USDT/1m');

                    // tell them its going to be ok.
                    code::json_die(['status'=>'pending']);

                } else {
                    code::json_die(['status'=>'perfect', 'res'=>$res]);
                }


        }
    
    }

}


function do_action(){

    if( $user = login_id() ){
        if( array_key_exists('do_action', $_REQUEST) ){
            
            switch( $_REQUEST['do_action'] ){

                case 'bot_stat':
                    code::json_die( bot_stat() );

                case 'symbolselect-feed':
                    if(! $ex = $_GET['ex'] or !class_exists($ex) ){
                        $result_s = ['null'];

                    } else {
                        $symbol_s = array_keys( ($ex)::exchangeInfo_pair() );
                        
                        if(! $req = $_REQUEST['symbol'] ){
                            $result_s = $symbol_s;
                        
                        } else foreach( $symbol_s as $symbol ){
                            if( stristr($symbol, $req) and $symbol != $req )
                                $result_s[] = $symbol;
                        }
                    }
                    code::json_die(['req'=>$req, 'res'=>$result_s]);

                case 'bot-remove':
                    bot_remove();
                    die;

                case 'sync-bot-params':
                    if( $bot = intval($_GET['id']) ){
                        if( $param = trim($_GET['param']) and in_array($param, ['valid_step', 'liq_margin', 'force_exit', 'save_perc', 'status']) ){
                            $value = trim($_GET['value']);
                            bot_rw($bot, [ $param => $value ]);
                            echo 'OK';
                            if( $param == 'status' )
                                echo '#evaldo_syncAPIList_setStat($(".botlist .re.r[bot='.$bot.']"),"'.$value.'");';
                            die;
                        }
                    }
                    die('ER');

                // case 'transfer-spare':
                //     code::json_die( bot_transfer_spare() );
                
                // case 'transfer-move':
                //     code::json_die( bot_transfer_move() );
                
                case 'backtest_save':
                    return backtest_save();
                        
            }

        }
    }

}



