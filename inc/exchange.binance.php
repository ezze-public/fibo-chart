<?php


/**
 * v-2.0 2024-04-05
 */

class binance
{


    public static function test(){

		# ezze main
		$credentials = 'sEZSjuoE0k1m3GVXJNyngg6EozKQTIYJJ16e1xyTygrwgee2dGfGoG2iqROphmoe:i5gJjRGXAbWy0n8RiqmEvA7jgTdVZ8cfhR1whVGBEXBq8xY8lp8ehtRRr0EOeO5A';

		# sub - weig - fibo
		// $credentials = 'SpH28IHjuH1GWS4GPIV2f6nLXiSY2Vl7oDRlyBOPLZRbzVXxbg3ukOpQzYQZQtsA:MERavBrULC7eFnbOZlqLYqm5mhoVwVcJmlRD4rGJ9kiV7iuxaLGkBurFDmKwihXT';
        

		// echo self::set_volume('btc_usdt', 0.0214819);
		// echo "<br>";
		// echo self::set_price('btc_usdt', 68229.123456);
		
		echo "<pre>";
		
		var_dump(

			// self::balance($credentials)
			// self::max_leverage($credentials, 'BTC_USDT')
			// self::leverageBracket_pair($credentials, "btcusdt")
			// self::order_list($credentials, 'dydx_usdt', 'open-long')
			// self::order_status($credentials, 'dot_usdt', 22985430484)
			// self::order_cancel($credentials, 'dot_usdt', 22983898073)
			// self::close_all_orders($credentials, 'dot_usdt')
			// self::order($credentials, 'fok', 'open-long', 'dot_usdt', 1, 8.60)
			// self::order($credentials, 'limit', 'close-long', 'dot_usdt', 1, 8.78)
			// self::order($credentials, 'fok', 'close-long', 'dot_usdt', 1, 8.725)
			// self::position_list($credentials, 'eth_usdt')
			// self::close_all_positions($credentials, 'eth_usdt')

			// self::spot_balance($credentials, 'USDT', false)
			// self::leverageBracket_read($credentials)
			// self::set_dual( $credentials, false )
			// self::set_leverage( $credentials, 'ETH_USDT', 210 )
			// self::order_list($credentials, 'DYDX_USDT', 'open-long')

			$price = self::set_price('BTCUSDT', '70132.60293')

		);

    }


	public static function balance( $credentials, $coin='', $only_available=false ){

		if( $res = self::request($credentials, 'GET /fapi/v2/balance', [ 'timestamp'=>self::curr_time() ]) ){

			foreach( $res as $item ){
				if( floatval($item['balance']) > 0 ){
					$balance_s[ $item['asset'] ] = $item['balance'];
					$available_s[ $item['asset'] ] = $item['maxWithdrawAmount'];
				}
			}

			if( $coin = strtoupper($coin) ){

				return $only_available
					? ( array_key_exists($coin, $available_s) ? $available_s[$coin] : 0 )
					: ( array_key_exists($coin, $balance_s) ? $balance_s[$coin] : 0 )
					;

			} else {
				return $only_available
					? $available_s
					: $balance_s
					;
			}

		}

		return false;
				
	}


	public static $side_translate = [
		'long' => 'buy',
		'sell' => 'sell',
	];

	/* { 
		"symbol": "DOTUSDT",
		"side": "BUY",
		"type": "LIMIT",
		"timeInForce": "GTC",
		"quantity": "100",
		"timestamp": 1712500038956,
		"price": "8.1"
	} */
	public static function order( $credentials, $type, $dimention, $symbol, $volume, $price=null, $flag_s = [] ){

		if( $volume <= 0 ){
			log_sys( __FUNCTION__ . ':' . __LINE__ );
			return false;
		}

		$symbol = self::clearSymbolName($symbol);

		if(! $max_volume = self::exchangeInfo_pair($symbol)['maxQty'] ){
			log_sys("Can't load exchangeInfo for {$symbol}");
			
		} else {
			
			# big orders
			if( $volume > $max_volume ){

				log_sys(" volume: $volume > max_volume: $max_volume, Can't cover it.");
				die;

				// while( true ){
				// 	$part_volume = ( ($volume <= $max_volume) ? $volume : ($max_volume / 2) );
				// 	$res[] = self::order($credentials, $type, $dimention, $symbol, $price, $part_volume);
				// 	$volume-= $part_volume;
				// 	if(! $volume )
				// 		return $res;
				// }

			}
		}

		# $dimention : open-sell, close-sell, open-long, close-long 
		list($close, $side) = explode('-', strtolower($dimention));
		$close = ($close == 'close');

		if( $close )
			$side = self::side_reverse($side);

		$type = strtolower($type);

		if(! $close ){
			// log_sys("$volume * $price");
			$the_amount = $volume * ( ($type == 'market') ? self::price($credentials, $symbol) : $price );
			if( $the_amount < 1 ){
				log_sys("the volume {$volume} of {$symbol} is lower than the threshold ({$the_amount} USDT)");
				if(! in_array('RE', $flag_s) ){
					fibo_chart::disconnect();
				}
			}
			$volume = self::set_volume($symbol, $volume);
		}

		log_this('logg', __FUNCTION__.':'.__LINE__.', volume '.$volume); // debug
		$volume = wash_my_float($volume);
		log_this('logg', __FUNCTION__.':'.__LINE__.', volume '.$volume); // debug

		if( $type != 'market' )
			$price = self::set_price($symbol, $price);

		if(! $volume ){
            log_sys("order for $volume $symbol is not possible");
			fibo_chart::disconnect();
		}

        switch( $type ){
            
            case 'limit': 
				$order_type = 'LIMIT';
                $time_in_force = 'GTC'; // GoodTillCancel
                break;

            case 'fok':
				$order_type = 'LIMIT';
                $time_in_force = 'FOK'; // FillOrKill
                break;

            case 'market':
				$order_type = 'MARKET';
                // $time_in_force = 'GTC'; // GoodTillCancel
                break;

        }

		$params =  [
			'symbol' => $symbol,
			'side' => strtoupper(self::$side_translate[$side]),
			'type' => $order_type,
			'quantity' => $volume,
			'timestamp' => self::curr_time(),
		];

		if( $time_in_force )
			$params['timeInForce'] = $time_in_force;
		
		if( $type != 'market' )
			$params['price'] = $price;

		if( $close )
			$params['reduceOnly'] = 'true';


		$res_order = self::request($credentials, 'POST /fapi/v1/order', $params);

		if( !array_key_exists('orderId', $res_order) or !$order_id = $res_order['orderId'] )
			return false;

		switch( $type ){

            case 'limit': 
                return $res_order['code'] ? false : $order_id;

            case 'fok':
                return $res_order['code'] ? false : $order_id;

            case 'market':
                return $res_order['code'] ? false : $order_id;

		}

	}
	/*  {
		"orderId": 22985394386,
		"symbol": "DOTUSDT",
		"status": "NEW",
		"clientOrderId": "8Y1ik7aYioqVQhgFKfKdJa",
		"price": "8.100",
		"avgPrice": "0.00",
		"origQty": "100.0",
		"executedQty": "0.0",
		"cumQty": "0.0",
		"cumQuote": "0.0000",
		"timeInForce": "GTC",
		"type": "LIMIT",
		"reduceOnly": false,
		"closePosition": false,
		"side": "BUY",
		"positionSide": "BOTH",
		"stopPrice": "0.000",
		"workingType": "CONTRACT_PRICE",
		"priceProtect": false,
		"origType": "LIMIT",
		"priceMatch": "NONE",
		"selfTradePreventionMode": "NONE",
		"goodTillDate": 0,
		"updateTime": 1712500039922
	} */


    /** 
	 * array(1) {
		[0]=>
		array(24) {
			["orderId"]=>
			int(22983898073)
			["symbol"]=>
			string(7) "DOTUSDT"
			["status"]=>
			string(3) "NEW"
			["clientOrderId"]=>
			string(24) "web_ZZWyxLrtUgsKFW1hhmVC"
			["price"]=>
			string(5) "8.110"
			["avgPrice"]=>
			string(1) "0"
			["origQty"]=>
			string(5) "166.4"
			["executedQty"]=>
			string(1) "0"
			["cumQuote"]=>
			string(6) "0.0000"
			["timeInForce"]=>
			string(3) "GTC"
			["type"]=>
			string(5) "LIMIT"
			["reduceOnly"]=>
			bool(false)
			["closePosition"]=>
			bool(false)
			["side"]=>
			string(3) "BUY"
			["positionSide"]=>
			string(4) "BOTH"
			["stopPrice"]=>
			string(1) "0"
			["workingType"]=>
			string(14) "CONTRACT_PRICE"
			["priceProtect"]=>
			bool(false)
			["origType"]=>
			string(5) "LIMIT"
			["priceMatch"]=>
			string(4) "NONE"
			["selfTradePreventionMode"]=>
			string(4) "NONE"
			["goodTillDate"]=>
			int(0)
			["time"]=>
			int(1712466233225)
			["updateTime"]=>
			int(1712466233225)
		}
		}
	 */
	public static function order_list( $credentials, $symbol=null, $dimention=null ){

		/* dimention: open-long, close-sell */

		if( $symbol )
			$params['symbol'] = self::clearSymbolName($symbol);
		$params['timestamp'] = self::curr_time();
		$params['recvWindow'] = 6000;

		$res = self::request($credentials, 'GET /fapi/v1/openOrders', $params);

		if(! is_array($res) ){
			log_sys("order_list: cant get result from /fapi/v1/openOrders");
			die;
		}
		
		if( $dimention ){

			list($close, $side) = explode('-', strtolower($dimention));
			$reduce_only = ($close == 'close');

			if( $reduce_only )
				$side = self::side_reverse($side);

			$side = self::$side_translate[$side];
			
			foreach( $res as $i => $o )
				if( $o['reduceOnly'] != $reduce_only or strtolower($o['side']) != $side )
					unset($res[$i]);
			
		}

		return $res;

	}


	public static function order_status( $credentials, $symbol, $order_id ){

		// NEW, CANCELED
		
        $res = self::request($credentials, 'GET /fapi/v1/order', [
            'symbol' => self::clearSymbolName($symbol),
            'orderId' => $order_id,
			'timestamp' => self::curr_time()
	    ]);

		return $res['code']
			? false
			: $res
			;

	}


	public static function order_cancel( $credentials, $symbol, $order_id ){
		
        $res = self::request($credentials, 'DELETE /fapi/v1/order', [
            'symbol' => self::clearSymbolName($symbol),
            'orderId' => $order_id,
			'timestamp' => self::curr_time()
	    ]);

		return $res['code']
			? false
			: $res
			;
		
	}


	public static function close_all_orders( $credentials, $symbol ){

		$res = self::request($credentials, 'DELETE /fapi/v1/allOpenOrders', [
            'symbol' => self::clearSymbolName($symbol),
			'timestamp' => self::curr_time()
	    ]);
		
		return $res['code'] == 200 
			? true 
			: false
			;

	}


	public static function position_list( $credentials, $symbol ){
		
		$res = self::request($credentials, 'GET /fapi/v2/positionRisk', [
			'symbol' => self::clearSymbolName($symbol),
			'timestamp' => self::curr_time()
		]);

		if( sizeof($res) != 1 )
			return false;
		else 
			$res = $res[0];

		$r['position'] = false;

		if( $res['positionAmt'] != 0 ){
			$r['position'] = $res['positionAmt'] > 0 ? 'long' : 'sell';
			$r['amount'] = abs($res['positionAmt']);
			$r['entry_price'] = self::set_price($symbol, $res['entryPrice']);
			$r['liq_price'] = self::set_price($symbol, $res['liquidationPrice']);
		}

		$r['res'] = $res;

		return $r;
		
	}


	public static function close_all_positions( $credentials, $symbol ){

		$r = self::position_list($credentials, $symbol);

		if( $r['position'] ){
			$dimention = "close-".$r['position'];
			$volume = $r['amount'];
			self::order($credentials, 'market', $dimention, $symbol, $volume);
		}

	}


	public static function price( $credentials, $symbol ){
		
		if( $res = self::request($credentials, 'GET /fapi/v1/ticker/price', [ 'symbol' => self::clearSymbolName($symbol) ]) )
			if( isset($res['price']) )
				return $res['price'];
		
		return false;
		
	}


	public static function set_dual( $credentials, $dual ){

		if( $dual === false or $dual === true ){
			$dual = $dual ? 'true' : 'false';
			$res = self::request($credentials, 'POST /fapi/v1/positionSide/dual', [
				'dualSidePosition' => $dual,
				'timestamp' => self::curr_time()
			]);
			return in_array($res['code'], [200, -4059]);
		}
		
		return false;

	}


	public static function set_leverage( $credentials, $symbol, $leverage ){
		
		$symbol = self::clearSymbolName($symbol);

		$max_leverage = self::leverageBracket_pair($credentials, $symbol);
		$leverage = min($leverage, $max_leverage);

		$res = self::request($credentials, 'POST /fapi/v1/leverage', [ 
			'symbol' => $symbol,
			'leverage' => $leverage,
			'timestamp' => self::curr_time()
		]);

		return array_key_exists('leverage', $res) 
			? $res['leverage']
			: false
			;

	}


	// public static function transfer( $credentials, $coin, $volume, $target ){

	// 	$sha1 = sha1(uniqid().rand(1000,9999));
	// 	$transfer_id = 
	// 		'selfTransfer_'.
	// 		substr($sha1, 0,8).'-'.
	// 		substr($sha1, 8,4).'-'.
	// 		substr($sha1,12,4).'-'.
	// 		substr($sha1,16,4).'-'.
	// 		substr($sha1,20,12);
		
	// 	$volume = strval($volume);

	// 	$target = strtoupper($target);
	// 	$source = ($target == 'SPOT') ? 'CONTRACT' : 'SPOT';

	// 	var_dump([ 
	// 		'transfer_id' => $transfer_id,
	// 		'coin' => $coin,
	// 		'amount' => $volume,
	// 		'from_account_type' => $source,
	// 		'to_account_type' => $target,
	// 	]);

	// 	return self::request($credentials, 'JSON', '/asset/v1/private/transfer', [ 
	// 		'transfer_id' => $transfer_id,
	// 		'coin' => $coin,
	// 		'amount' => $volume,
	// 		'from_account_type' => $source,
	// 		'to_account_type' => $target,
	// 	]);

	// }


	public static function spot_balance( $credentials, $coin, $only_available=false ){
		
		$res = self::request($credentials, 'api GET /api/v3/account', [ 
			'omitZeroBalances' => 'true',
			'timestamp' => self::curr_time()
		]);
		
		if( $res && array_key_exists('balances', $res) )
			foreach( $res['balances'] as $balance )
				if( $balance['asset'] == $coin )
					return $only_available
						? $balance['free']
						: $balance['free'] + $balance['locked']
						;

		return 0;

	}


	public static function set_price( $symbol, $price ){
		
		$ex_info = self::exchangeInfo_pair( self::clearSymbolName($symbol) );

		$ps = $ex_info['pricePrecision']; // BTC 2
		$powed_ps = pow(10, $ps);
		$price = floor( $price * $powed_ps ) / $powed_ps;

		$ts = $ex_info['tickSize']; // BTC 0.10
		$price = floor($price / $ts) * $ts;

		$price = strval($price);
		
		return $price;
		
	}


	public static function set_volume( $symbol, $volume ){
		
		$mq = self::exchangeInfo_pair( self::clearSymbolName($symbol) )['minQty']; // BTC 0.001
		$z = strval($volume / $mq);
		$volume = floor($z) * $mq;
		$volume = strval($volume);

		return $volume;

	}


	public static function min_volume( $symbol ){
		$mq = self::exchangeInfo_pair( self::clearSymbolName($symbol) )['minQty']; // BTC 0.001
		return $mq;
	}

	
	private static $exchangeInfo_path = '/tmp/perp-fibo-trader/exchangeInfo.binance';
	private static $exchangeInfo_link = 'https://fapi.binance.com/fapi/v1/exchangeInfo';
	private static $leverageBracket_path = '/tmp/perp-fibo-trader/leverageBracket.binance';


	public static function exchangeInfo_pair( $symbol=null ){

		if( $symbol ){
			$symbol = trim(strtoupper($symbol));
			$symbol = self::clearSymbolName($symbol);
			$symbol = substr($symbol, 0, -4) .'_'. substr($symbol, -4);
		}

		if( $data = self::exchangeInfo_read() ){

			foreach( $data as $item ){
				
				if(! in_array($item['quoteAsset'], [ 'USDT', 'BUSD', 'USDC' ]) )
					continue;

				if( $item['status'] != 'TRADING' )
					continue;
				if( $item['contractType'] != 'PERPETUAL' )
					continue;

				$pair = $item['baseAsset'].'_'.$item['quoteAsset'];
				$symbol_s[$pair]['pricePrecision'] = $item['pricePrecision'];

				if( $symbol and $symbol != $pair )
					continue;
				
				foreach( $item as $k => $r ){
					if( $k != 'filters' )
						continue;
					foreach( $r as $r_chunk ){
						
						if( $r_chunk['filterType'] == 'LOT_SIZE' ){
							$symbol_s[$pair]['minQty'] = $r_chunk['minQty'];
							$symbol_s[$pair]['maxQty'] = $r_chunk['maxQty'];
						
						} else if( $r_chunk['filterType'] == 'PRICE_FILTER' ){
							$symbol_s[$pair]['tickSize'] = $r_chunk['tickSize'];
						}
					}
				}

				if( $symbol and $symbol == $pair )
					return $symbol_s[$pair];
				
			}

			ksort($symbol_s);
			return $symbol_s;

		}

		return false;

	}
	
	
	private static function exchangeInfo_read( $symbol = null ){
		
		if( self::exchangeInfo_sync() ){

			$data = file_get_contents(self::$exchangeInfo_path);
			$res = json_decode($data, true)['symbols'];

			if( $symbol = self::clearSymbolName($symbol) ){
				
				foreach( $res as $item )
					if( $item['symbol'] == $symbol )
						return $item;

			} else {
				return $res;
			}

		}

		return false;

	}


	private static function exchangeInfo_sync(){
		
		$file = self::$exchangeInfo_path;
		
		if(! file_exists($file) or filemtime($file) < date('U') - 3600*24 )
			sys::safe_file_copy(self::$exchangeInfo_link, $file);
		
		return file_exists($file);

	}


	public static function leverageBracket_pair( $credentials, $symbol=null ){

		if( $symbol )
			$symbol = self::clearSymbolName($symbol);

		foreach( self::leverageBracket_read($credentials) as $item )
			$leverage_s[ $item['symbol'] ] = $item['brackets'][0]['initialLeverage'];
		
		return $symbol
			? ( array_key_exists($symbol, $leverage_s)
				? $leverage_s[$symbol]
				: 0 )
			: $leverage_s;

	}


	private static function leverageBracket_read( $credentials ){
		self::leverageBracket_sync($credentials);
		$file = self::$leverageBracket_path;
		$data = file_get_contents($file);
		$res = json_decode($data, true);
		return $res;
	}


	private static function leverageBracket_sync( $credentials ){
		$file = self::$leverageBracket_path;
		if(! file_exists($file) or filemtime($file) < date('U') - 3600*24 ){
			$data = self::request($credentials, 'GET /fapi/v1/leverageBracket', [ 'timestamp' => self::curr_time() ]);
			$data = json_encode($data);
			file_put_contents($file, $data);
		}
	}

	
    private static function side_reverse( $side ){
		
		$side = strtolower($side);
		
		if( in_array($side, ['long', 'buy']) )
			return 'sell';
		
		else if( in_array($side, ['short', 'sell']) )
			return 'long';
		
		else
			return false;

    }


	private static function request( $credentials, $method_n_path, $params=[], $retry=0 ){

		# die if retry passed
		if( $retry >=20 ){
			log_sys('request:'.__LINE__.':'.$retry.' - '.$method_n_path.' - '.json_encode($params));
			die;
		}

		if( array_key_exists('timestamp', $params) )
			$params['timestamp'] = self::curr_time();

		log_sys('request:'.__LINE__.':'.$retry.' - '.$method_n_path.' - '.json_encode($params)); // debug

		try {

			list($api_key, $api_secret) = explode(':', $credentials);

			switch( substr_count($method_n_path, ' ') ){
				case 2:
					list($api, $method, $path) = explode(' ', $method_n_path);
					break;
				case 1:
					$api = 'fapi';
					list($method, $path) = explode(' ', $method_n_path);
					break;
				default:
					log_sys("wrong content");
					die;
			}

			$params_query = http_build_query($params);
			$signature = shell_exec(' echo -n "'.$params_query.'" | openssl dgst -sha256 -hmac "'.$api_secret.'" ');
			$signature = trim( explode('(stdin)= ', $signature)[1] , "\r\n\t ");

			$res_str = shell_exec(" curl -H 'X-MBX-APIKEY: {$api_key}' -X {$method} 'https://{$api}.binance.com{$path}?{$params_query}&signature={$signature}' 2>/dev/null ");
			log_sys('request:'.__LINE__.':'.$retry.' - '.$method_n_path.' - '.$res_str);

			$res_str = trim($res_str, "\r\n\t ");

			if( $res_str == '' ){
				log_sys('request:'.__LINE__.':'.$retry.' - '.$method_n_path.' - '.json_encode($params).' -> '.$res_str);
				sleep(3);
				return self::request($credentials, $method_n_path, $params, $retry+1);
				
			} else {

				$res = json_decode($res_str, true);
				
				# if request failed
				if( array_key_exists('code', $res) and in_array($res['code'], [ -1021 ]) ){
					log_sys('request:'.__LINE__.':'.$retry.' - '.$method_n_path.' - '.json_encode($params).' -> '.$res_str);
					sleep(3);
					return self::request($credentials, $method_n_path, $params, $retry+1);
					
				} else {
					return $res;
				}
				
			}

		} catch (Exception $e){
			log_sys('request:'.__LINE__.':'.$retry.' - '.$method_n_path.' e '.$e->getMessage());
		}

		log_sys('request:'.__LINE__.':'.$retry.' - '.$method_n_path); // debug

		return false;

	}


	private static function curr_time(){
		return floor(microtime(true) * 1000);
	}


	private static function clearSymbolName( $symbol ){
		$symbol = trim($symbol, "\r\n\t ");
		$symbol = str_replace('_', '', $symbol);
		$symbol = strtoupper($symbol);
		return $symbol;
	}


}

