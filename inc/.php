<?php

# v1.1 2022-11-14

ini_set('display_errors', 'On');
error_reporting(E_ALL & ~E_NOTICE);
$reserved_files = [ 'dotenv' ];

chdir('/var/www');

foreach( $reserved_files as $file )
	if( file_exists('inc/'.$file.'.php') )
		include_once('inc/'.$file.'.php');

foreach( glob('inc/*.php') as $file )
	if(! in_array($file, get_included_files()) )
		include_once($file);

global $dp;

$caCertPath = '/var/www/ssl/ca-certificate.crt';

// Initialize the MySQLi connection
$dp = mysqli_init();

// Set the SSL options
mysqli_ssl_set($dp, NULL, NULL, $caCertPath, NULL, NULL);

// Attempt to establish a secure connection to the MySQL server
if (!mysqli_real_connect($dp, MYSQL_HOST, MYSQL_USER, MYSQL_PASS, MYSQL_NAME, MYSQL_PORT, NULL, MYSQLI_CLIENT_SSL)) {
    die("Can't connect to the MySQL server securely. Error: " . mysqli_connect_error());
# } else {
#     echo "Connected to the MySQL server securely using SSL.";
}

# if(! $dp = mysqli_connect( MYSQL_HOST, MYSQL_USER, MYSQL_PASS, MYSQL_NAME ) )
# 	die("Can't connect to the MySQL server.");

