<?php


function botlist_log(){
    // die;
    ?>
    <html>
        <head>
            <title>fibo-chart</title>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
            <link rel="stylesheet" href="/lib/log.css">
            <script src="//code.jquery.com/jquery-3.6.1.min.js"></script>
        </head>
        <?php
        
        if( $bot = intval($_GET['bot']) ){

            if( $proc = $_GET['proc'] ){
                
                if( $proc == 'latest' )
                    if(! $proc = botlist_latest_proc($bot) ){
                        die("no proc found");
                    
                    } else {
                        echo "<script>location.href = '/?do_action_pre=log&bot={$bot}&proc=$proc';</script>";
                        die;
                    }
                                
                return botlist_log_view($bot, $proc);
            
            } else if( $user = login_id() ){
                return botlist_log_list($bot);
            }

        }

        ?>
    </html>
    <?php

}


function botlist_latest_proc( $bot ){
    
    if( $rw = dbqf(" SELECT `id` FROM `proc` WHERE `bot`=$bot ORDER BY `id` DESC LIMIT 1 ") ){
        return $rw['id'];
    
    } else {
        return false;
    }

}


function botlist_log_list( $bot ){

    $proc_s = dbqf(" SELECT * FROM `proc` WHERE `bot`=$bot ORDER BY `id` DESC ");
    
    if(! sizeof($proc_s) ){
        echo "no proc to view";

    } else {

        ?>
        <body>
        <div class="botlist_log_list">
            <div class="re h">
                <span class="link"><a href="./?do_action_pre=log&bot=<?= $bot ?>&proc=<?= $id ?>"><?= $date_start ?></a></span>
                <span>round</span>
                <span>profit</span>
                <span>percent</span>
                <span>specs</span>
            </div>
            <?
        
            foreach( $proc_s as $proc ){
                
                extract($proc);

                $specs = [];

                if( $bot_specs ){
                    $bot_specs = json_decode($bot_specs, true);
                    foreach( $bot_specs as $k => $v )
                        $specs[]= "$k $v";
                    if( sizeof($specs) )
                        $specs = implode(', ', $specs);
                
                } else {
                    $specs = '';
                }
                
                ?>
                <div class="re r">
                    <span class="link"><a href="./?do_action_pre=log&bot=<?= $bot ?>&proc=<?= $id ?>"><?= $date_start ?></a></span>
                    <span><?= $count_of_rounds ?></span>
                    <span><?= $total_profit ?></span>
                    <span><?= $total_percent ?>%</span>
                    <span class="specs" ><?= $specs ?></span>
                </div>
                <?

            }

            ?>
        </div>
        </body>
        <?

    }

}


function botlist_log_view( $bot, $proc ){

    $bot_rw = bot_rw($bot);
    
    ?>
    <body class="view" proc="<?php echo $proc ?>" >
        
        <bar><!--
         --><pair><?php echo explode('_', $bot_rw['symbol'])[0] ?></pair><!--
         --><price><!--
             --><curr>0000</curr><!--
             --><down>0000</down><!--
             --><up>0000</up><!--
             --><liq>0000</liq><!--
         --></price><!--
         --><profit><!--
             --><round><!--
                --><amount>0.00</amount><!--
                 --><percent>0.00</percent><!--
             --></round><!--
             --><total><!--
                 --><amount>00.00</amount><!--
                 --><percent>00.00</percent><!--
             --></total><!--
         --></profit><!--
     --></bar>

        <ul>
        <?php

        $rw_s = dbqf(" SELECT `id`, `text`, `seen` FROM `log` WHERE `bot`=$bot AND `proc`=$proc ORDER BY `id` DESC ");
        if( sizeof($rw_s) ){
            $proc_ptr = $rw_s[0]['id'];
            $class_s = 'new';
            foreach( $rw_s as $rw ){
                extract($rw);
                $class_s = [];
                if(! $seen )
                    $class_s[] = 'new';
                if( strstr($text, 'CLOSE WITH') )
                    $class_s[] = 'close';
                echo "<li".( sizeof($class_s) ? ' class="'.implode(' ', $class_s).'" ' :'').">{$text}</li>\n";
            }

        } else {
            $proc_ptr = 0;
        }

        ?>
        </ul>
    </body>

    <script>
        var proc = <?php echo $proc ?>;
        var proc_ptr = <?php echo $proc_ptr ?>;
    </script>
    <script src="/lib/tail.log.js"></script>
    <?php

}




