<?php


function do_page(){

    html_header();

    if(! isset($_REQUEST['do']) )
        $_REQUEST['do'] = 'list';
    
    switch($_REQUEST['do']){
        
        case 'api':
            apilist();
            break;

        case 'profile':
            do_profile();
            break;

        case 'bot':
        default:
            botlist();
            break;
            
    }

    html_footer();

}
