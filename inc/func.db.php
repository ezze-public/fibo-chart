<?php

# v-0.2 2024-10-08

function dbq( $q ){
	
	$res = mysql_query($q);

	if(! $res ){
		log_this('db.error', "QUERY: ".$q."\nERROR: ".dbe()."\n" );

	} else {
		return $res;
	}

}


function dbr($rs, $row, $col=null){
	return $col 
        ? mysql_result($rs, $row, $col)
        : mysql_result($rs, $row);
}


function dbc( $table, $ar=null ){
	$ar_str = iusd_where($ar);
	// echo $ar_str;
	return dbr( dbq(" SELECT COUNT(*) FROM `{$table}` WHERE 1 {$ar_str} "), 0, 0);
}


function dbn($rs){
	return mysql_num_rows($rs);
}


function dbf($rs){
	return mysql_fetch_assoc($rs);
}


function dbaf(){
	return mysql_affected_rows();
}


function dbrm( $table, $id ){
	$id = intval($id);
	return dbq(" DELETE FROM `{$table}` WHERE `id`={$id} LIMIT 1 ");
}


function dbe(){
	return mysql_error();
}


function dbi(){

	return mysql_insert_id();

}


function dbqf( $q, $die=false, $key_by_id=false ){

	# v-1.1 2022-11-14

	$qn = strtolower($q);
	while( strstr($qn, ' ') )
		$qn = str_replace(' ', '', $qn);
	$single_req = ( substr($qn,-6) == 'limit1' );
	
	$rw_s = [];

	if(! $rs = dbq($q) ){
		if( $die ) die;
		echo dbe();

	} else if(! dbn($rs) ){
		if( $die ) die;
		return $single_req 
			? null 
			: [];

	} else while( $rw = dbf($rs) ){
		
		if( $key_by_id ){
			
			if( is_bool($key_by_id) ){
				$key_by_id = 'id';
			
			 } else if( strstr($key_by_id, '>') ){
				list($key_by_id, $the_value) = explode('>', $key_by_id);
			}
			
			$id = $rw[$key_by_id];
			
			if( $the_value ){
				if( strstr($the_value, ',') ){
					$the_value = explode(',', $the_value);
					foreach( $the_value as $the_value_this )
						$rw_s[ $id ][] = $rw[$the_value_this];
				} else {
					$rw_s[ $id ] = $rw[$the_value];
				}

			} else {
				unset($rw[$key_by_id]);
				$rw_s[ $id ] = $rw;
			}

		} else {
			$rw_s[] = $rw;
		}
	}

	if( $single_req ){
		$rw_s = array_slice($rw_s,0,1);
		if( sizeof($rw_s) )
			$rw_s = $rw_s[0];
	}

	return $rw_s;

}


function dbin( $table, $arr ){

	$col_s = '`' . implode('`, `', array_keys($arr) ) . '`';
	$val_s = "'" . implode("', '", array_values($arr) ) . "'";
	
	return dbq(" INSERT INTO `$table` ($col_s) VALUES ($val_s) ")
		? dbi()
		: false;

}






function mysql_query( $q, $dp=null ){
	if(! $dp )
		global $dp;
	return mysqli_query($dp, $q);
}


function mysql_num_rows( $rs ){
	return mysqli_num_rows($rs);
}


function mysql_num_fields( $rs ){
	return mysqli_num_fields($rs);
}


function mysql_result($res, $row, $field=0){ 
    $res->data_seek($row); 
    $datarow = $res->fetch_array(); 
    return $datarow[$field]; 
}


function mysql_fetch_assoc($res){
	return mysqli_fetch_assoc($res);
}


function mysql_fetch_array($res){
	return mysqli_fetch_array($res);
}


function mysql_error(){
	global $dp;
	return mysqli_error($dp);
} 


function mysql_errno(){
	global $dp;
	return mysqli_errno($dp);
} 


function mysql_affected_rows(){
	global $dp;
	return mysqli_affected_rows($dp);
} 


function mysql_insert_id(){
	global $dp;
	return mysqli_insert_id($dp);
}


function mysql_close(){
	global $dp;
	mysqli_close($dp);
}


function mysql_real_escape_string( $str ){
	global $dp;
	return mysqli_real_escape_string($dp, $str);
}



