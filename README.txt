
#
# wget -qO- https://gitlab.com/ezze-public/perp-fibo-trader/-/raw/main/README.txt | bash -s perp-fibo-trader 1420

wget -O dockerfile https://gitlab.com/ezze-public/conf/-/raw/main/nginxphp-on-docker/conf/dockerfile
sed -i 's,#INJECT,RUN wget -qO- https://gitlab.com/ezze-public/perp-fibo-trader/-/raw/main/conf/patch.sh | sh,g' dockerfile

name=$1
port=$2
pssh=$((port-1))

mkdir -p /docker/$name
env_path=/docker/$name/.env
if [ ! -f $env_path ] || [ ! -s $env_path ]; then
    wget -qO- https://gitlab.com/ezze-public/perp-fibo-trader/-/raw/main/conf/.env-sample > /docker/$name/.env
fi

docker rm -f $name
docker build -t $name"-image" -f dockerfile .
rm -rf dockerfile

docker run -t -d --restart unless-stopped --name $name -p $pssh:22 -p $port:80 -v /docker/$name/.env:/var/www/.env -v /root/.ssh:/root/.ssh $name"-image"
docker exec -t $name sh -c " cd /var/www && git pull "
docker exec -t $name sh -c " sed -i 's,https://gitlab.com/,git@gitlab.com:,g' /var/www/.git/config "

